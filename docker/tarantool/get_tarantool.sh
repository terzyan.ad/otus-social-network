git clone https://github.com/tarantool/mysql-tarantool-replication.git mysql_tarantool-replication
cd mysql-tarantool-replication
git submodule update --init --recursive
cmake .
make