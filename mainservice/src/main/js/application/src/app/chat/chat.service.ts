import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Profile} from "../entity/profile";
import {PageEvent} from "@angular/material/paginator";
import {Chat} from "../entity/chat";
import {Message} from "../entity/message";
import {MsgContent} from "../entity/msgContent";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ChatService {

    PATH = '/chat/'

    constructor(private http: HttpClient) {
    }

    loadChats() {
        return this.http.get<Chat[]>(environment.apiUrl + environment.apiPath + this.PATH);
    }


    loadChatById(id: number) {
        return this.http.get<Chat>(environment.apiUrl + environment.apiPath + this.PATH + id);
    }

    getChatForUser(userId: number) {
        return this.http.get<Chat>(environment.apiUrl+environment.apiPath+this.PATH+'user'
            +"/"+userId
        );
    }

    searchChat(search: String) {
        return this.http.get<Chat[]>(environment.apiUrl+environment.apiPath+this.PATH+'search'
            +"/"+search
        );
    }

    send(chat: Chat, msg: string): Observable<any> {
        let content = new MsgContent()
        content.msg = msg;
        content.chatId = chat.id

        return this.http.post(
            environment.apiUrl+environment.apiPath+this.PATH,
            content);
    }
}