import {Component, OnInit, ViewChild} from '@angular/core';
import {ChatService} from "./chat.service";
import {Chat} from "../entity/chat";
import {ScrollToBottomDirective} from "./scroll-to-bottom.directive";
import {Message} from "../entity/message";

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

    // @ts-ignore
    @ViewChild(ScrollToBottomDirective)
    scroll: ScrollToBottomDirective;

    chats: Chat[]
    search = ''
    msg = ''

    selectChat?: Chat

    constructor(private chatService: ChatService) {
    }

    ngOnInit() {
        this.chatService.loadChats().subscribe((result) => {
            this.chats = result
        });
    }

    onSelect(chat: Chat): void {
        if (chat.id === 0)
            this.chatService.getChatForUser(chat.users[0].id).subscribe((result) => {
                this.selectChat = result
            })
        else
            this.chatService.loadChatById(chat.id).subscribe((result) => {
                this.selectChat = result
            });
    }

    searchChat(search: String) {
        if (search.length > 0)
            this.chatService.searchChat(search).subscribe((result) => {
                this.chats = result
            });
        else
            this.chatService.loadChats().subscribe((result) => {
                this.chats = result
            });
    }

    chatName(chat: Chat): string {
        if (chat.users.length > 1)
            return chat.name
        return chat.users[0].card.firstName + ' ' + chat.users[0].card.secondName
    }

    sendMessage(msg: string) {
        this.chatService.send(this.selectChat, msg).subscribe(() => {
            this.chatService.loadChatById(this.selectChat.id).subscribe((result) => {
                this.selectChat = result
                this.msg = ''
            });
        })
    }

    classOfMessage(msg: Message): string {
        console.log(msg.user)
        console.log(msg.user === null)
        if (msg.user === null)
            return "d-flex justify-content-end mb-4"
        else
            return "d-flex justify-content-start mb-4"
    }
}
