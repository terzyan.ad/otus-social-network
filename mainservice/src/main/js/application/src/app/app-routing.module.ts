import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from "./registration/registration.component";
import { AuthGuard } from "./interceptors/AuthGuard";
import {PeopleComponent} from "./people/people.component";
import {ChatComponent} from "./chat/chat.component";
import {PostComponent} from "./post/post.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'registration', component: RegistrationComponent },
  { path: 'logout', component: LoginComponent },
  { path: 'people', component: PeopleComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'post', component: PostComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
