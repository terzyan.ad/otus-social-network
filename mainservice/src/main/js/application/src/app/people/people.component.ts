import { Component, OnInit } from '@angular/core';
import { PeopleService } from "./people.service";
import { Profile } from "../entity/profile";
import {PageEvent} from "@angular/material/paginator";
import {filter, map} from "rxjs/operators";

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  peoples: Profile[]

  selectProfile?: Profile
  search='';
  length: number;
  pageIndex: number = 0;
  pageSize: number = 10;

  constructor(private peopleService: PeopleService) {
  }

  ngOnInit() {
    this.peopleService.loadProfiles().subscribe( (result) => {
      this.peoples = result;
    });
  }

  onSelect(profile: Profile): void {
    this.selectProfile = profile;
  }

  addToFriend(){
    this.peopleService.requestToFriendship(this.selectProfile)
  }

  searchPeople(search: String){
    this.peopleService.countSearchPeople(search)
        .pipe(
            filter(i=>i>0),
            map(i=>{
              this.length = i;
              this.peopleService.searchPeopleNew(search).subscribe( (result) => {
                this.peoples = result;
              })
            }))
        .subscribe()
  }

  changePage(event: PageEvent) {
    this.peopleService.searchPeople(this.search,event).subscribe( (result) => {
      this.peoples = result;
    });
  }
}
