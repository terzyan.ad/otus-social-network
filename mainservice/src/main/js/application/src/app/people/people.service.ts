import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Profile} from "../entity/profile";
import {PageEvent} from "@angular/material/paginator";
@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  PATH = '/profile'
  PATH_FRIEND = '/friend'

  constructor(private http: HttpClient) { }

  loadProfiles() {
    return this.http.get<Profile[]>(environment.apiUrl+environment.apiPath+this.PATH+'/all');
  }

  loadFriends() {
    return this.http.get<Profile[]>(environment.apiUrl+environment.apiPath+this.PATH_FRIEND+"?type=1");
  }

  requestToFriendship(profile: Profile) {
    console.log("Add to friend")
    this.http.post(
      environment.apiUrl+environment.apiPath+this.PATH_FRIEND,
      profile).subscribe();
    console.log("Add to friend send")
  }

  searchPeopleNew(search: String) {
    return this.http.get<Profile[]>(environment.apiUrl+environment.apiPath+this.PATH+'/search'
        +"?first_name="+search
        +"&second_name="+search
        +"&sort=second_name,desc"
        +"&page=0"
        +"&size=10"
    );
  }

  searchPeople(search: String,event: PageEvent) {
    return this.http.get<Profile[]>(environment.apiUrl+environment.apiPath+this.PATH+'/search'
        +"?first_name="+search
        +"&second_name="+search
        +"&sort=second_name,desc"
        +"&page="+event.pageIndex
        +"&size="+event.pageSize
    );
  }

  countSearchPeople(search: String) {
    return this.http.get<number>(environment.apiUrl+environment.apiPath+this.PATH+'/count_search'
        +"?first_name="+search
        +"&second_name="+search
    );
  }
}
