export class PostContent {
    title: string;
    content: string;
}