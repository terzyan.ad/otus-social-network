export class UserCard {

    id: number
    firstName: string;
    secondName: string;
    sex: string;
    age: number;
    city: string;
    interest: string;
}