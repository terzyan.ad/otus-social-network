import {User} from "./user";

export class Post {
    userId: number;
    user: User;
    title: string;
    content: string;
    lastUpdate: number;
}