import {User} from "./user";
import {Message} from "./message";

export class Chat {
    id: number;
    name: string;
    lastMessage: string;
    messages: Message[];
    users: User[];
    mapUsers: Record<string,User>
}