export class RegistrationCard {

    mail: string;
    password: string;
    firstName: string;
    secondName: string;
    sex: string;
    age: number;
    city: string;
    interest: string;
}
