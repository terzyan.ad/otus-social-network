import {User} from "./user";

export class Message {
    userId: number;
    user: User;
    msg: string;
    lastUpdate: number;
}