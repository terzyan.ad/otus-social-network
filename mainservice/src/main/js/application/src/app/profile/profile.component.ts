import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import {Profile} from "../entity/profile";
import {PeopleService} from "../people/people.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile: Profile;
  friends?: Profile[];

  constructor(private profileService: ProfileService,
              private peopleService: PeopleService) { }

  ngOnInit() {
    this.profileService.loadProfile().subscribe( (result) => {
      this.profile = result;
    });

    this.peopleService.loadFriends().subscribe( (result) => {
      this.friends = result;
    });
  }

}
