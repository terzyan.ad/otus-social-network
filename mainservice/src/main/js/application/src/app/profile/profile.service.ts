import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Profile } from '../entity/profile';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  PATH = '/profile'

  constructor(private http: HttpClient) { }

  loadProfile() {
    return this.http.get<Profile>(environment.apiUrl+environment.apiPath+this.PATH);
  }


}
