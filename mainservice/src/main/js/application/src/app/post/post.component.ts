import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {PostService} from "./post.service";
import {Post} from "../entity/post";
import {PostContent} from "../entity/postContent";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {environment} from "../../environments/environment";
import {Stomp} from "@stomp/stompjs";
import * as SockJS from 'sockjs-client';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {


    posts: Post[]
    sessionId = '';
    disabled = true;
    private stompClient = null;

    constructor(private postService: PostService,
                public dialog: MatDialog) {
    }

    ngOnInit() {
        this.postService.loadPosts().subscribe((result) => {
            this.posts = result
        });
        this.connect()
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(DialogPost, {
            width: '650px',
            data: PostContent,
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            console.log(result);
            this.sendMessage(result)
        });
    }

    sendMessage(content: PostContent) {
        this.postService.send(content);
    }

    connect() {

        const socket = new SockJS(environment.apiUrl +'/ws');
        this.stompClient = Stomp.over(socket);
        const _this = this;

        this.stompClient.connect({}, function (frame) {
            var url = _this.stompClient.ws._transport.url;
            url = url.replace(
                "ws://localhost:8088/ws/",  "");
            url = url.replace("/websocket", "");
            url = url.replace(/^[0-9]+\//, "");
            console.log("Your current session is: " + url);
            _this.sessionId = url;

            _this.stompClient.send(
                '/sn/post',
                {},
                JSON.stringify({ 'check': 'true' })
            );

            _this.stompClient.subscribe('/user/' + _this.sessionId+'/', function (post) {
                _this.posts.unshift(JSON.parse(post.body))
            });
        })
    }
}


@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'post_dialog.component.html'
})
export class DialogPost{
    constructor(
        public dialogRef: MatDialogRef<DialogPost>,
        @Inject(MAT_DIALOG_DATA) public data: PostContent,
    ) {}

    onNoClick(): void {
        this.dialogRef.close();
    }
}
