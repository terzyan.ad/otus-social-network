import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from "rxjs";
import {Post} from "../entity/post";
import {PostContent} from "../entity/postContent";
import {MsgContent} from "../entity/msgContent";

@Injectable({
    providedIn: 'root'
})
export class PostService {

    PATH = '/post/'

    constructor(private http: HttpClient) {
    }

    loadPosts() {
        return this.http.get<Post[]>(environment.apiUrl + environment.apiPath + this.PATH);
    }

    send(content: PostContent){

        let res = new PostContent()
        res.title = content.title;
        res.content = content.content
        console.log(res)
        this.http.post(
            environment.apiUrl+environment.apiPath+this.PATH,
            res).subscribe();
    }
}