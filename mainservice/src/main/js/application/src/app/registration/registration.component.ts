import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { RegistrationService } from './registration.service';
import { RegistrationCard } from "../entity/registrationCard";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  card: RegistrationCard = new RegistrationCard();

  constructor(private registrationService: RegistrationService,
              private location: Location,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
  }

  handleRegistration() {
    this.registrationService.registrationService(this.card).subscribe((result) => {
      this.router.navigate(['/profile']);
    });
  }

  back(): void {
    this.location.back();
  }

}
