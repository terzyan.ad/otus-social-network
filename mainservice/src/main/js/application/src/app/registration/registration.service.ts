import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {RegistrationCard} from "../entity/registrationCard";

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  PATH = '/registration'

  constructor(private http: HttpClient) { }

  registrationService(userCard: RegistrationCard) {
    return this.http.post(
      environment.apiUrl+environment.apiPath+this.PATH,
      userCard);
  }
}
