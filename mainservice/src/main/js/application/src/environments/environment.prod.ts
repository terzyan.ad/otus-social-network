export const environment = {
  production: true,
  apiUrl: 'API_URL',
  apiPath: '/api/v1'
};
