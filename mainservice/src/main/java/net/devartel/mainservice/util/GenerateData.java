package net.devartel.mainservice.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.dto.RegistrationCard;
import net.devartel.api.service.RegistrationService;
import net.devartel.api.util.NameHolder;
import net.devartel.api.util.SurnameHolder;
import org.springframework.stereotype.Component;

@Slf4j
public class GenerateData {
    private final static int STEP = 1000;
    private final static int BATCH = 100;

    private final RegistrationService service;

    private final List<NameHolder> names;
    private final List<SurnameHolder> surnames;

    public GenerateData(RegistrationService service) throws IOException {
        this.service = service;
        ObjectMapper mapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        names = mapper.readValue(new File("mainservice/src/main/resources/data/russian_names.json"), new TypeReference<>(){});
        surnames =  mapper.readValue(new File("mainservice/src/main/resources/data/russian_surnames.json."), new TypeReference<>(){});
    }

    public void generate(long count, int multiThread) {

        Executor executor = Executors.newFixedThreadPool(multiThread);
        AtomicLong countOfRow = new AtomicLong(1200000);

        for (int i = 0; i < count; i++) {
            Runnable runnable = () -> {
                String threadNum = Thread.currentThread().getName();

                final long calculatedCount = countOfRow.getAndAdd(STEP);

                log.warn("Thread {} start generate", threadNum);
                long time = System.currentTimeMillis();
                List<RegistrationCard> collection = IntStream.range(0, STEP)
                    .mapToObj(val -> randomObject(val, calculatedCount))
                    .collect(Collectors.toList());
                time = System.currentTimeMillis() - time;
                log.warn("Thread {} complete generate by {} millis", threadNum, time);
                List<List<RegistrationCard>> sl = new ArrayList<>();

                for(int index = 0; index< STEP/BATCH;index++){
                    int start = index * BATCH;
                    int end = start + BATCH;
                    sl.add(collection.subList(start,end));
                }

                time = System.currentTimeMillis();
                log.warn("Thread {} start persist", threadNum);
                for(List<RegistrationCard> l:sl){
                    service.registration(l);
                }
                time = System.currentTimeMillis() - time;
                log.warn("Thread {} complete persist by {} seconds", threadNum, time / 1000);
            };

            executor.execute(runnable);
        }
    }

    private RegistrationCard randomObject(int id, long delta){
        int r = new Random(System.currentTimeMillis()+id).ints(0, 50000)
            .findFirst()
            .getAsInt();
        NameHolder h = names.get(r);
        int index = r%2==0? surnames.size() - r : r;
        if(index>300000 || index < 0)
            index = 100;
        SurnameHolder s = surnames.get(index);
        RegistrationCard card = new RegistrationCard();
        card.setFirstName(h.getName());
        card.setSecondName(s.getSurname());
        card.setAge((short) r);
        card.setCity("Default");
        card.setMail("user_"+(delta+id));
        card.setPassword("user");
        card.setInterest("none");
        card.setSex(h.getSex().equals("М")?"Male":"Female");

        return card;
    }
}
