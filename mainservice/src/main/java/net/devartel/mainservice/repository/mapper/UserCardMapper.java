package net.devartel.mainservice.repository.mapper;

import java.sql.PreparedStatement;
import java.util.StringJoiner;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserCard;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserCardMapper implements RowMapper<UserCard>, PreparedStatementMapper<UserCard> {

    public static final String TABLE_NAME = "user_card";

    public static final String FIELD_ID = "id";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_FIRST_NAME = "first_name";
    public static final String FIELD_SECOND_NAME = "second_name";
    public static final String FIELD_MAIL = "mail";
    public static final String FIELD_SEX = "sex";
    public static final String FIELD_CITY = "city";
    public static final String FIELD_INTEREST = "interest";
    public static final String FIELD_AGE = "age";

    @Override
    public UserCard mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserCard userCard = new UserCard();

        userCard.setId(rs.getLong(FIELD_ID));
        userCard.setMail(rs.getString(FIELD_MAIL));
        userCard.setFirstName(rs.getString(FIELD_FIRST_NAME));
        userCard.setSecondName(rs.getString(FIELD_SECOND_NAME));
        userCard.setSex(rs.getString(FIELD_SEX));
        userCard.setCity(rs.getString(FIELD_CITY));
        userCard.setInterest(rs.getString(FIELD_INTEREST));
        userCard.setAge(rs.getShort(FIELD_AGE));
        userCard.setUser(new User(rs.getLong(FIELD_USER_ID)));

        return userCard;
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + TABLE_NAME + " (" +
            new StringJoiner(",")
                .add(UserCardMapper.FIELD_USER_ID)
                .add(UserCardMapper.FIELD_MAIL)
                .add(UserCardMapper.FIELD_FIRST_NAME)
                .add(UserCardMapper.FIELD_SECOND_NAME)
                .add(UserCardMapper.FIELD_SEX)
                .add(UserCardMapper.FIELD_CITY)
                .add(UserCardMapper.FIELD_INTEREST)
                .add(UserCardMapper.FIELD_AGE)
            + ") VALUES (?,?,?,?,?,?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE  " + TABLE_NAME + " SET " +
            UserCardMapper.FIELD_USER_ID        + "= ?, " +
            UserCardMapper.FIELD_MAIL           + "= ?, " +
            UserCardMapper.FIELD_FIRST_NAME     + "= ?, " +
            UserCardMapper.FIELD_SECOND_NAME    + "= ?, " +
            UserCardMapper.FIELD_SEX            + "= ?, " +
            UserCardMapper.FIELD_CITY           + "= ?, " +
            UserCardMapper.FIELD_INTEREST       + "= ?, " +
            UserCardMapper.FIELD_AGE            + "= ? " +
            "WHERE " + UserCardMapper.FIELD_ID  + "= ?";
    }

    @Override
    public PreparedStatement mapStatementInsert(PreparedStatement statement, UserCard entity) throws SQLException {
        statement.setLong(1, entity.getUser().getId());
        statement.setString(2, entity.getMail());
        statement.setString(3, entity.getFirstName());
        statement.setString(4, entity.getSecondName());
        statement.setString(5, entity.getSex());
        statement.setString(6, entity.getCity());
        statement.setString(7, entity.getInterest());
        statement.setShort(8, entity.getAge());
        return statement;
    }

    @Override
    public PreparedStatement mapStatementUpdate(PreparedStatement statement, UserCard entity) throws SQLException {
        statement = mapStatementInsert(statement, entity);
        statement.setLong(9, entity.getId());
        return statement;
    }

    @Override
    public String getFullSelectQuery() {
        return "SELECT * FROM " + TABLE_NAME + " WHERE " +
            UserCardMapper.FIELD_USER_ID        + "= ? AND " +
            UserCardMapper.FIELD_MAIL           + "= ? AND " +
            UserCardMapper.FIELD_FIRST_NAME     + "= ? AND " +
            UserCardMapper.FIELD_SECOND_NAME    + "= ? AND " +
            UserCardMapper.FIELD_SEX            + "= ? AND " +
            UserCardMapper.FIELD_CITY           + "= ? AND " +
            UserCardMapper.FIELD_INTEREST       + "= ? AND " +
            UserCardMapper.FIELD_AGE            + "= ? ";
    }
}
