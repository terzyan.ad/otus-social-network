package net.devartel.mainservice.repository.mapper;

import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserRelation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRelationMapper implements RowMapper<UserRelation> {

    public static final String FIELD_ID = "id";
    public static final String FIELD_REQUEST_USER_ID = "request_user_id";
    public static final String FIELD_TARGET_USER_ID = "target_user_id";
    public static final String FIELD_RELATION_TYPE = "relation_type";

    @Override
    public UserRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserRelation userRelation = new UserRelation();

        userRelation.setId(rs.getLong(FIELD_ID));
        userRelation.setRequestUser(new User(rs.getLong(FIELD_REQUEST_USER_ID)));
        userRelation.setTargetUser(new User(rs.getLong(FIELD_TARGET_USER_ID)));
        userRelation.setRelationType(RelationType.valueOfLabel(rs.getInt(FIELD_RELATION_TYPE)));

        return userRelation;
    }
}
