package net.devartel.mainservice.repository.impl;

import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserCard;
import net.devartel.api.repository.UserCardRepository;
import net.devartel.api.repository.impl.BaseRepositoryImpl;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import net.devartel.mainservice.repository.mapper.UserCardMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Slf4j
@Component
public class RegistrationRepositoryImpl extends BaseRepositoryImpl<UserCard,Long> implements UserCardRepository {

    private static final String TABLE_NAME = "user_card";

    private final UserCardMapper mapper = new UserCardMapper();

    public RegistrationRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public RowMapper<UserCard> getRowMapper() {
        return mapper;
    }

    @Override
    public PreparedStatementMapper<UserCard> getStatementMapper() {
        return mapper;
    }

    @Override
    public UserCard getByUser(User user) {

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + UserCardMapper.FIELD_USER_ID + " = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, user.getId());

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                return new UserCardMapper().mapRow(resultSet, 0);
            else
                return null;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    public Collection<UserCard> search(Map<String,String> clause, Pageable page) {

        StringBuilder query = new StringBuilder("SELECT * FROM " + TABLE_NAME);
            if(clause.size()>0){
                query.append(" WHERE ");
                int count = 0;
                for(String fieldName:clause.keySet()){
                    count++;
                    query.append(fieldName).append(" LIKE ?");
                    if(count < clause.size()){
                        query.append(" OR ");
                    }
                }
            }

            if(page.getSort().isSorted()){
                query.append(" ORDER BY ");
                page.getSort().get()
                    .forEach(order -> query.append(order.getProperty()).append(" ").append(order.getDirection()));
            }

            if(page.isPaged()){
                int limit = page.getPageSize();
                int offset = limit * page.getPageNumber();

                query.append(" LIMIT ").append(limit).append(" OFFSET ").append(offset);
            }

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query.toString())) {
            int i = 1;
            for (String value : clause.values())
                statement.setString(i++, value+"%");

            ResultSet resultSet = statement.executeQuery();

            List<UserCard> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next())
                result.add(new UserCardMapper().mapRow(resultSet, row++));

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public long countSearch(Map<String, String> clause) {

        StringBuilder query = new StringBuilder("SELECT count(*) FROM " + TABLE_NAME);
        if(clause.size()>0){
            query.append(" WHERE ");
            int count = 0;
            for(String fieldName:clause.keySet()){
                count++;
                query.append(fieldName).append(" LIKE ?");
                if(count < clause.size()){
                    query.append(" OR ");
                }
            }
        }

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query.toString())) {
            int i = 1;
            for (String value : clause.values())
                statement.setString(i++, value+"%");

            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next())
                return resultSet.getInt(1);

            return 0;
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }
}
