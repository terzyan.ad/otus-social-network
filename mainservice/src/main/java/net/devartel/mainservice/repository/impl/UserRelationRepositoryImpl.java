package net.devartel.mainservice.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserRelation;
import net.devartel.api.repository.BaseRepository;
import net.devartel.api.repository.UserRelationRepository;
import net.devartel.mainservice.repository.mapper.UserRelationMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserRelationRepositoryImpl implements UserRelationRepository {

    private static final String TABLE_NAME = "user_relation";

    private final DataSource dataSource;

    @Override
    public Optional<UserRelation> getEntityById(Long aLong) {

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + BaseRepository.FIELD_ID + " = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, aLong);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.ofNullable(new UserRelationMapper().mapRow(resultSet, 0));
            } else {
                return Optional.empty();
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            return Optional.empty();
        }
    }

    @Override
    public Collection<UserRelation> getEntityByInIds(Collection<Long> ids) {

        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + BaseRepository.FIELD_ID + " IN  (?"
            + ", ?".repeat(ids.size() - 1) + ")";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            int i = 1;
            for (long id : ids) {
                statement.setLong(i++, id);
            }

            ResultSet resultSet = statement.executeQuery();

            List<UserRelation> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next()) {
                result.add(new UserRelationMapper().mapRow(resultSet, row++));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<UserRelation> getAll() {
        String query = "SELECT * FROM " + TABLE_NAME;
        Collection<UserRelation> result = new HashSet<>();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                result.add(new UserRelationMapper().mapRow(resultSet, resultSet.getRow()));
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            result = new HashSet<>();
        }
        return result;
    }

    @Override
    public UserRelation save(UserRelation entity) {
        if (entity.getId() == null) {

            String query = "INSERT INTO " + TABLE_NAME + " (" +
                new StringJoiner(",")
                    .add(UserRelationMapper.FIELD_REQUEST_USER_ID)
                    .add(UserRelationMapper.FIELD_TARGET_USER_ID)
                    .add(UserRelationMapper.FIELD_RELATION_TYPE)
                + ") VALUES (?,?,?)";

            try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setLong(1, entity.getRequestUser().getId());
                statement.setLong(2, entity.getTargetUser().getId());
                statement.setLong(3, entity.getRelationType().ordinal());

                long resultSet = statement.executeUpdate();
                if (resultSet == 0) {
                    log.warn("Запрос на создание связи пользователя {} невыполнился",
                        entity.getRequestUser().getLogin());
                }
                connection.commit();
            } catch (SQLException ex) {
                log.error(ex.getLocalizedMessage());
                throw new RuntimeException(ex);
            }
        } else {

            String query = "UPDATE  " + TABLE_NAME + " SET " +
                UserRelationMapper.FIELD_REQUEST_USER_ID + "= ?, " +
                UserRelationMapper.FIELD_TARGET_USER_ID + "= ?, " +
                UserRelationMapper.FIELD_RELATION_TYPE + "= ? " +
                "WHERE " + BaseRepository.FIELD_ID + "= ?";

            try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setLong(1, entity.getTargetUser().getId());
                statement.setLong(2, entity.getRequestUser().getId());
                statement.setLong(3, entity.getRelationType().ordinal());
                statement.setLong(4, entity.getId());

                ResultSet resultSet = statement.executeQuery();
                if (resultSet == null) {
                    log.warn("Запрос на обновление связи пользователя {} невыполнился",
                        entity.getRequestUser().getLogin());
                }
                connection.commit();
            } catch (SQLException ex) {
                log.error(ex.getLocalizedMessage());
                throw new RuntimeException(ex);
            }
        }
        return entity;
    }

    @Override
    public Collection<UserRelation> save(Collection<UserRelation> entities) {
        return entities;
    }

    @Override
    public void remove(UserRelation entity) {

        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + UserRelationMapper.FIELD_ID + "= ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, entity.getId());

            ResultSet resultSet = statement.executeQuery();
            if (resultSet == null) {
                log.warn("Запрос на удаление связи пользователя {} невыполнился", entity.getRequestUser().getLogin());
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<UserRelation> getRelationByUserAndTypes(User user, RelationType... types) {

        String query = "SELECT * FROM " + TABLE_NAME
            + " WHERE (" + UserRelationMapper.FIELD_REQUEST_USER_ID + "= ? "
            + " OR " + UserRelationMapper.FIELD_TARGET_USER_ID + "= ?) ";

        if (Objects.nonNull(types) && types.length > 0) {
            query += " AND " + UserRelationMapper.FIELD_RELATION_TYPE + " IN ( ? "
                + ", ?".repeat(types.length - 1) + ")";
        }

        Collection<UserRelation> result = new HashSet<>();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, user.getId());
            statement.setLong(2, user.getId());

            int index = 3;
            Iterator<RelationType> it = Arrays.stream(types).iterator();
            while (it.hasNext()) {
                statement.setLong(index++, it.next().ordinal());
            }

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                UserRelation userRelation = new UserRelationMapper().mapRow(resultSet, resultSet.getRow());
                if (Objects.nonNull(userRelation)) {
                    result.add(userRelation);
                }
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
        }
        return result;
    }

    @Override
    public Collection<UserRelation> getRelationByTargetUser(User requester, User target) {
        return getRelationByUserAndTypes(requester).stream()
            .filter(relation -> relation.getTargetUser().getId().equals(target.getId()))
            .peek(relation -> relation.setTargetUser(target))
            .collect(Collectors.toList());
    }
}
