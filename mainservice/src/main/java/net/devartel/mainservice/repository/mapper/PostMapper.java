package net.devartel.mainservice.repository.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;
import net.devartel.api.domain.Post;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import org.springframework.jdbc.core.RowMapper;

public class PostMapper implements RowMapper<Post>, PreparedStatementMapper<Post> {

    public static final String TABLE_NAME = "post";

    public static final String FIELD_ID = "id";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_CONTENT = "content";
    public static final String FIELD_LAST_UPDATE = "last_update";

    @Override
    public Post mapRow(ResultSet rs, int rowNum) throws SQLException {
        Post post = new Post();

        post.setId(rs.getLong(FIELD_ID));
        post.setTitle(rs.getString(FIELD_TITLE));
        post.setContent(rs.getString(FIELD_CONTENT));
        post.setUserId(rs.getLong(FIELD_USER_ID));
        post.setLastUpdate(rs.getLong(FIELD_LAST_UPDATE));

        return post;
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + TABLE_NAME + " (" +
            new StringJoiner(",")
                .add(PostMapper.FIELD_USER_ID)
                .add(PostMapper.FIELD_TITLE)
                .add(PostMapper.FIELD_CONTENT)
                .add(PostMapper.FIELD_LAST_UPDATE)
            + ") VALUES (?,?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE  " + TABLE_NAME      + " SET " +
            PostMapper.FIELD_USER_ID        + "= ?, " +
            PostMapper.FIELD_TITLE          + "= ?, " +
            PostMapper.FIELD_CONTENT        + "= ?, " +
            PostMapper.FIELD_LAST_UPDATE    + "= ? "  +
            "WHERE " + PostMapper.FIELD_ID  + "= ?";
    }

    @Override
    public PreparedStatement mapStatementInsert(PreparedStatement statement, Post entity) throws SQLException {
        statement.setLong(1, entity.getUserId());
        statement.setString(2, entity.getTitle());
        statement.setString(3, entity.getContent());
        statement.setLong(4, System.currentTimeMillis()/1000);
        return statement;
    }

    @Override
    public PreparedStatement mapStatementUpdate(PreparedStatement statement, Post entity) throws SQLException {
        statement = mapStatementInsert(statement, entity);
        statement.setLong(5, entity.getId());
        return statement;
    }

    @Override
    public String getFullSelectQuery() {
        return "SELECT * FROM  " + TABLE_NAME + " WHERE " +
            PostMapper.FIELD_USER_ID     + "= ? AND " +
            PostMapper.FIELD_TITLE       + "= ? AND " +
            PostMapper.FIELD_CONTENT     + "= ? AND " +
            PostMapper.FIELD_LAST_UPDATE + "= ? ";
    }
}
