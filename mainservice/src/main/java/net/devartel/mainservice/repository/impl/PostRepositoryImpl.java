package net.devartel.mainservice.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.Post;
import net.devartel.api.repository.PostRepository;
import net.devartel.api.repository.impl.BaseRepositoryImpl;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import net.devartel.mainservice.repository.mapper.PostMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PostRepositoryImpl extends BaseRepositoryImpl<Post, Long> implements PostRepository {

    private final PostMapper mapper;

    public PostRepositoryImpl(DataSource dataSource) {
        super(dataSource);
        this.mapper = new PostMapper();
    }

    @Override
    public String getTableName() {
        return "post";
    }

    @Override
    public RowMapper<Post> getRowMapper() {
        return mapper;
    }

    @Override
    public PreparedStatementMapper<Post> getStatementMapper() {
        return mapper;
    }

    @Override
    public Collection<Post> getPostsByUserIdIn(Collection<Long> ids, Pageable page) {
        if(Objects.isNull(ids) || ids.isEmpty())
            return Collections.emptyList();


        StringBuilder query = new StringBuilder("SELECT * FROM " + getTableName() + " WHERE " + PostMapper.FIELD_USER_ID + " IN  (?"
            + ", ?".repeat(ids.size() - 1) + ")");

        if(Objects.nonNull(page)) {
            if (page.getSort().isSorted()) {
                query.append(" ORDER BY ");
                page.getSort().get()
                    .forEach(order -> query.append(order.getProperty()).append(" ").append(order.getDirection()));
            }

            if (page.isPaged()) {
                int limit = page.getPageSize();
                int offset = limit * page.getPageNumber();

                query.append(" LIMIT ").append(limit).append(" OFFSET ").append(offset);
            }
        }

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query.toString())) {
            int i = 1;
            for (Long id : ids) {
                setNeededParam(statement, i++, id);
            }

            ResultSet resultSet = statement.executeQuery();

            List<Post> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next()) {
                result.add(getRowMapper().mapRow(resultSet, row++));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<Post> getAll(Collection<Long> ids, Pageable page) {

        StringBuilder query = new StringBuilder("SELECT * FROM " + getTableName()).append(" WHERE ")
            .append(PostMapper.FIELD_USER_ID).append(" IN  (?").append(", ?".repeat(ids.size() - 1)).append(")");

        if(page.getSort().isSorted()){
            query.append(" ORDER BY ");
            page.getSort().get()
                .forEach(order -> query.append(order.getProperty()).append(" ").append(order.getDirection()));
        }

        if(page.isPaged()){
            int limit = page.getPageSize();
            int offset = limit * page.getPageNumber();

            query.append(" LIMIT ").append(limit).append(" OFFSET ").append(offset);
        }

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query.toString())) {
            int i = 1;
            for (Long id : ids) {
                setNeededParam(statement, i++, id);
            }

            ResultSet resultSet = statement.executeQuery();

            List<Post> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next())
                result.add(getRowMapper().mapRow(resultSet, row++));

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }
}
