package net.devartel.mainservice.controller;

import lombok.AllArgsConstructor;
import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.service.FriendshipService;
import net.devartel.mainservice.util.Constants;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(Constants.API_URL + "/friend")
@AllArgsConstructor
public class FriendshipController {

    private final FriendshipService service;

    @GetMapping
    public Collection<Profile> getRelatedUsersByType(@RequestParam(value = "type", defaultValue = "1") Integer type) {
        return service.getUserByRelationType(RelationType.valueOfLabel(type));
    }

    @PostMapping
    public void sendFriendRequest(@RequestBody Profile profile) {
        service.requestToFriendship(profile);
    }

    @PostMapping("/accept")
    public void acceptFriendRequest(@RequestBody Profile profile) {
        service.acceptFriendship(profile);
    }

    @PostMapping("/block")
    public void blockUser(@RequestBody Profile profile) {
        service.blockUser(profile);
    }

    @PostMapping("/unblock")
    public void unblockUser(@RequestBody Profile profile) {
        service.unblockUser(profile);
    }

    @GetMapping("/all")
    public Collection<Profile> getAll() {
        return service.getAll();
    }

    @GetMapping("/by-type/{type}")
    public Collection<Profile> getByType(@PathVariable String type) {
        return service.getUserByRelationType(RelationType.valueOf(type));
    }

    @GetMapping("/subscribed")
    public Collection<Profile> getSubscribed() {
        return service.getSubscribed();
    }
}