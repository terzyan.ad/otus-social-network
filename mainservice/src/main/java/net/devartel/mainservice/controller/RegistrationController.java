package net.devartel.mainservice.controller;

import lombok.AllArgsConstructor;
import net.devartel.api.domain.dto.RegistrationCard;
import net.devartel.api.service.RegistrationService;
import net.devartel.mainservice.util.Constants;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Constants.API_URL + "/registration")
@AllArgsConstructor
public class RegistrationController {

    private final RegistrationService service;
//    private final GenerateData generateData;

    @PostMapping()
    public String greeting(@RequestBody RegistrationCard card) {
        return service.registration(card);
    }

    @GetMapping("/gen/{count}")
    public void generate(@RequestParam("count") int count, @RequestParam("thread") int thread) {
//        generateData.generate(count, thread);
    }
}