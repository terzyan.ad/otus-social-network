package net.devartel.mainservice.controller;

import java.util.Collection;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.dto.Chat;
import net.devartel.api.domain.dto.ChatContent;
import net.devartel.api.service.ChatService;
import net.devartel.mainservice.util.Constants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Constants.API_URL + "/chat")
@AllArgsConstructor
public class ChatController {

    private final ChatService service;

    @GetMapping("/")
    public Collection<Chat> getChats() {
        return service.getAllChats();
    }

    @GetMapping("/{id}")
    public Optional<Chat> getChatById(@PathVariable long id) {
        return service.getChatById(id);
    }

    @GetMapping("/user/{userId}")
    public Collection<Chat> getChatByUserId(@PathVariable long userId) {
        return service.getChatByUserId(userId);
    }

    @GetMapping("/search/{name}")
    public Collection<Chat> search(@PathVariable String name) {
        return service.search(name);
    }

    @PostMapping("/")
    public void receiveContent(@RequestBody ChatContent msg) {
        service.receiveContent(msg);
    }

}