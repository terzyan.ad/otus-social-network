package net.devartel.mainservice.controller;

import java.util.Arrays;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.service.ProfileService;
import net.devartel.mainservice.util.Constants;
import org.springframework.data.domain.Pageable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Slf4j
@RestController
@RequestMapping(Constants.API_URL + "/profile")
@AllArgsConstructor
public class ProfileController {

    private final ProfileService service;

    @GetMapping()
    public Profile getProfile() {
        return service.currentProfile();
    }

    @GetMapping("/all")
    public Collection<Profile> getAll(Pageable page) {
        return service.getAllProfiles();
    }

    @GetMapping("/search")
    public Collection<Profile> search(
        @RequestParam Map<String, String> clause,
        Pageable page) {
        return service.search(clause, page);
    }

    @GetMapping("/count_search")
    public long countSearch(
        @RequestParam Map<String, String> clause) {
        return service.countSearch(clause);
    }

    @RequestMapping(value = "/id/{userIds}", method = RequestMethod.GET)
    public Collection<Profile> getByUserIds(@PathVariable Long[] userIds) {
        return service.getProfilesByUserId(Arrays.asList(userIds));
    }

}