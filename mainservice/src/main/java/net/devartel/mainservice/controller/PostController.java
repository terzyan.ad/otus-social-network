package net.devartel.mainservice.controller;

import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.Post;
import net.devartel.api.domain.dto.PostContent;
import net.devartel.api.service.PostService;
import net.devartel.mainservice.util.Constants;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Constants.API_URL + "/post")
@AllArgsConstructor
public class PostController {

    private final PostService service;

    @GetMapping("/")
    public Collection<Post> getPosts(Pageable page) {
        return service.getAllPosts(page);
    }

    @GetMapping("/{id}")
    public Post getPost(@PathVariable Long id) {
        return service.getPostById(id).orElseThrow();
    }

    @PostMapping("/")
    public void receiveContent(@RequestBody PostContent msg) {
        service.receiveContent(msg);
    }

}