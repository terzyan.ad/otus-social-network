package net.devartel.mainservice.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.RequiredArgsConstructor;
import net.devartel.api.domain.User;
import net.devartel.api.repository.UserRepository;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class SocketController {

    private final Map<Long, Collection<String>> userSessions = new ConcurrentHashMap<>();

    private final SimpMessagingTemplate simpMessagingTemplate;

    private final UserRepository userRepository;

    @MessageMapping("/post")
    public void greeting(@Payload String msg,
        Principal user,
        @Header("simpSessionId") String sessionId) throws Exception {

        User currentUser = userRepository.getByLogin(user.getName());

        if (!userSessions.containsKey(currentUser.getId())) {
            userSessions.put(currentUser.getId(), new ArrayList<>());
        }

        userSessions.get(currentUser.getId()).add(sessionId);
    }

    public void sendToUser(Long userId, String msg) {
        for (String session : userSessions.getOrDefault(userId, Collections.emptyList())) {
            simpMessagingTemplate.convertAndSendToUser(
                session, "", msg);
        }

    }

}
