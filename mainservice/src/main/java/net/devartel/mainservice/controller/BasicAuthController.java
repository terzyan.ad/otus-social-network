package net.devartel.mainservice.controller;

import net.devartel.api.domain.AuthenticationBean;
import net.devartel.mainservice.util.Constants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constants.API_URL)
public class BasicAuthController {

	@GetMapping(path = "/basicauth")
	public AuthenticationBean authResult() {
		return new AuthenticationBean("You are authenticated");
	}	
}
