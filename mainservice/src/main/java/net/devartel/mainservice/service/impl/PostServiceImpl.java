package net.devartel.mainservice.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.Post;
import net.devartel.api.domain.dto.PostContent;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.repository.PostRepository;
import net.devartel.api.repository.UserRepository;
import net.devartel.api.security.AppSecurityContext;
import net.devartel.api.service.FriendshipService;
import net.devartel.api.service.PostService;
import net.devartel.api.service.ProfileService;
import net.devartel.mainservice.service.mq.PostSender;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Primary
@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    public static final int SIZE_OF_CACHE = 10;

    private final UserRepository userRepository;

    private final ProfileService profileService;

    private final PostRepository postRepository;

    private final AppSecurityContext securityContext;

    private final FriendshipService friendshipService;

    private final RedisTemplate<Long, Long> postTemplate;

    private final PostSender postSender;

    @Override
    public Collection<Post> getAllPosts(Pageable page) {
        Collection<Post> posts;

        if (page.isPaged() && page.getPageNumber() * page.getPageSize() < SIZE_OF_CACHE) {
            posts = getPostFromCache(page);

            if (!posts.isEmpty()) {
                return posts;
            }
        }

        Collection<Long> subscribed = friendshipService.getSubscribed()
            .stream().map(profile -> profile.getCard().getId()).collect(Collectors.toSet());

        posts = postRepository.getPostsByUserIdIn(subscribed, page);
        return posts.stream()
            .peek(post -> post.setUser(
                profileService.getProfilesByUserId(Collections.singleton(post.getUserId())).iterator().next()))
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Post> getPostById(long id) {
        Optional<Post> post = postRepository.getEntityById(id);

        if (post.isEmpty()) {
            return Optional.empty();
        }
        return post.stream().peek(p -> p.setUser(
                profileService.getProfilesByUserId(Collections.singleton(p.getUserId())).iterator().next()))
            .findFirst();
    }

    @Override
    public Collection<Post> getPostByUserId(long userId, Pageable page) {
        Collection<Post> posts = postRepository.getPostsByUserIdIn(Collections.singleton(userId), page);
        return posts.stream()
            .peek(post -> post.setUser(
                profileService.getProfilesByUserId(Collections.singleton(post.getUserId())).iterator().next()))
            .collect(Collectors.toList());
    }

    @Override
    public void receiveContent(PostContent content) {
        Profile currentUser = profileService.currentProfile();

        Post post = new Post(content);
        post.setProfile(currentUser);

        post = postRepository.save(post);

        List<Long> subscribed = friendshipService.getSubscribed()
            .stream().map(profile -> profile.getCard().getUser().getId()).toList();

        for (Long idSubscriber : subscribed) {
            if (Boolean.TRUE.equals(postTemplate.hasKey(idSubscriber))
                && postTemplate.opsForList().size(idSubscriber) > SIZE_OF_CACHE)
                postTemplate.opsForList().leftPop(idSubscriber);

            postTemplate.opsForList().leftPush(idSubscriber, post.getId());
            try {
                post.setProfile(currentUser);
                postSender.send(idSubscriber,post);
            }catch (Exception ex){
                log.error(ex.getMessage());
            }
        }

    }

    private Collection<Post> getPostFromCache(Pageable page) {
        Profile currentUser = profileService.currentProfile();

        if (Boolean.FALSE.equals(postTemplate.hasKey(currentUser.getId()))) {
            return Collections.emptyList();
        }

        long startIndex = (long) page.getPageNumber() * page.getPageSize();
        long endIndex = startIndex + page.getPageSize();

        if (postTemplate.opsForList().size(currentUser.getId()) < endIndex) {
            endIndex = postTemplate.opsForList().size(currentUser.getId());
        }

        return postTemplate.opsForList().range(currentUser.getId(), startIndex, endIndex).stream()
            .map(postRepository::getEntityById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .peek(p -> p.setUser(
                profileService.getProfilesByUserId(Collections.singleton(p.getUserId())).iterator().next()))
            .collect(Collectors.toSet());
    }
}
