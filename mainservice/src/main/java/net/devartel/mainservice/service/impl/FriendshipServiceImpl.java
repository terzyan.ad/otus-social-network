package net.devartel.mainservice.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserRelation;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.repository.UserRelationRepository;
import net.devartel.api.repository.UserRepository;
import net.devartel.api.service.FriendshipService;
import net.devartel.api.service.ProfileService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Primary
@Service
@RequiredArgsConstructor
public class FriendshipServiceImpl implements FriendshipService {

    private final UserRelationRepository relationRepository;

    private final UserRepository userRepository;

    private final ProfileService profileService;

    @Override
    public void requestToFriendship(Profile profile) {
        profile = profileService.fillProfile(profile);
        Optional<UserRelation> relation = findRelationForCurrentProfile(profile);

        if (relation.isPresent() &&
            Arrays.asList(RelationType.CANDIDATE, RelationType.FRIEND).contains(relation.get().getRelationType())) {
            log.info("Request for user {} already have", profile.getUserLogin());
            return;
        }

        UserRelation currentRelation = createRelation(profile);
        currentRelation.setRelationType(RelationType.CANDIDATE);

        relationRepository.save(currentRelation);
    }

    @Override
    public void acceptFriendship(Profile profile) {
        Optional<UserRelation> relation = findRelationWithCurrentProfile(profile);

        if (relation.isEmpty()) {
            log.info("Interacted record for user {} not found", profile.getUserLogin());
            return;
        }

        UserRelation currentRelation = relation.get();

        if (currentRelation.getRelationType().equals(RelationType.CANDIDATE)) {

            currentRelation.setRelationType(RelationType.FRIEND);
            relationRepository.save(currentRelation);

            UserRelation friendRelation = createRelation(profile);
            friendRelation.setRelationType(RelationType.FRIEND);
            relationRepository.save(friendRelation);
        }
    }

    @Override
    public void rejectFriendship(Profile profile) {
        Optional<UserRelation> relation = findRelationForCurrentProfile(profile);

        if (relation.isEmpty()) {
            log.info("Interacted record for user {} not found", profile.getUserLogin());
            return;
        }

        UserRelation currentRelation = relation.get();

        if (Arrays.asList(RelationType.CANDIDATE, RelationType.FRIEND).contains(relation.get().getRelationType())) {
            relationRepository.remove(currentRelation);
            return;
        }

        log.info("Type of relation can't be switch off");

    }

    @Override
    public void blockUser(Profile profile) {
        Optional<UserRelation> relation = findRelationForCurrentProfile(profile);

        if (relation.isPresent()) {
            if (relation.get().getRelationType().equals(RelationType.BLOCKED)) {
                log.info("User already blocked");
                return;
            } else {
                relationRepository.remove(relation.get());
            }
        }

        UserRelation currentRelation = createRelation(profile);
        currentRelation.setRelationType(RelationType.BLOCKED);

        relationRepository.save(currentRelation);
    }

    @Override
    public void unblockUser(Profile profile) {

        Optional<UserRelation> relation = findRelationForCurrentProfile(profile);

        if (relation.isEmpty()) {
            log.info("No interacted record found for user {}", profile.getUserLogin());
            return;
        }

        if (!relation.get().getRelationType().equals(RelationType.BLOCKED)) {
            log.info("User wasn't blocked");
        } else {
            relationRepository.remove(relation.get());
        }
    }

    @Override
    public Collection<Profile> getUserByRelationType(RelationType type) {
        User requester = userRepository.getByLogin(profileService.currentProfile().getUserLogin());
        return relationRepository.getRelationByUserAndTypes(requester, type).stream()
            .peek(this::fillRelation)
            .map(relation -> profileService.getProfile(relation.getTargetUser()))
            .collect(Collectors.toList());
    }

    @Override
    public Collection<Profile> getSubscribed() {
        Collection<Profile> subscribed = getUserByRelationType(RelationType.CANDIDATE);
        User requester = userRepository.getByLogin(profileService.currentProfile().getUserLogin());

        subscribed.addAll(
            relationRepository.getRelationByUserAndTypes(requester, RelationType.FRIEND).stream()
                .peek(this::fillRelation)
                .map(relation -> relation.getTargetUser().equals(requester)? relation.getRequestUser(): relation.getTargetUser())
                .map(profileService::getProfile)
                .collect(Collectors.toSet())
        );
        return subscribed;
    }

    @Override
    public Collection<Profile> getAll() {
        return relationRepository.getAll().stream()
            .peek(this::fillRelation)
            .map(relation -> profileService.getProfile(relation.getRequestUser()))
            .collect(Collectors.toList());
    }

    private UserRelation createRelation(Profile profile) {

        User requester = userRepository.getByLogin(profileService.currentProfile().getUserLogin());
        Optional<User> target = userRepository.getEntityById(profile.getCard().getUser().getId());

        if (target.isPresent()) {
            UserRelation relation = new UserRelation();
            relation.setRequestUser(requester);
            relation.setTargetUser(target.get());
            return relation;
        } else {
            throw new NullPointerException(
                "User with ID" + profile.getCard().getId() + " not found");
        }

    }

    private Optional<UserRelation> findRelationForCurrentProfile(Profile profile) {
        User requester = userRepository.getByLogin(profileService.currentProfile().getUserLogin());
        Optional<User> target = userRepository.getEntityById(profile.getCard().getUser().getId());

        return findRelation(target.get(), requester);
    }


    private Optional<UserRelation> findRelationWithCurrentProfile(Profile profile) {
        User requester = userRepository.getByLogin(profile.getUserLogin());
        User target = userRepository.getByLogin(profileService.currentProfile().getUserLogin());

        return findRelation(target, requester);

    }

    private Optional<UserRelation> findRelation(User target, User requester) {
        Collection<UserRelation> relations = relationRepository
            .getRelationByTargetUser(requester, target);

        if (relations.isEmpty()) {
            return Optional.empty();
        }

        if (relations.size() == 1) {
            return Optional.of(fillRelation(relations.iterator().next()));
        }

        log.error("Uncorrected count of user relation");

        throw new IllegalStateException(
            "More then one record for " + requester.getLogin() + " and " + target.getLogin());

    }

    private UserRelation fillRelation(UserRelation relation) {
        Optional<User> requestUser = userRepository.getEntityById(relation.getRequestUser().getId());
        Optional<User> targetUser = userRepository.getEntityById(relation.getTargetUser().getId());

        if (requestUser.isEmpty() || targetUser.isEmpty()) {
            log.info("No interacted record found for relation {}", relation.getId());
            throw new NullPointerException("No interacted record found for relation #" + relation.getId());
        }

        relation.setRequestUser(requestUser.get());
        relation.setTargetUser(targetUser.get());

        return relation;

    }
}
