package net.devartel.mainservice.service.impl;

import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserCard;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.repository.UserCardRepository;
import net.devartel.api.repository.UserRepository;
import net.devartel.api.security.AppSecurityContext;
import net.devartel.api.service.ProfileService;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.NonNull;

import java.util.Collection;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Slf4j
@Primary
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UserRepository userRepository;

    private final UserCardRepository userCardRepository;

    private final AppSecurityContext securityContext;

    public ProfileServiceImpl(UserRepository userRepository,
        UserCardRepository userCardRepository,
        AppSecurityContext securityContext) {
        this.userRepository = userRepository;
        this.userCardRepository = userCardRepository;
        this.securityContext = securityContext;
    }

    @Override
    public Profile currentProfile() {
        return getProfile(userRepository.getByLogin(securityContext.getCurrentUsername()));
    }

    @Override
    public Collection<Profile> getAllProfiles() {
        return userRepository.getAll().stream()
            .filter(user -> !user.getLogin().equals(securityContext.getCurrentUsername()))
            .map(this::getProfile)
            .collect(Collectors.toList());
    }

    @Override
    public Collection<Profile> getProfilesByUserId(Collection<Long> userId) {
        return userRepository.getEntityByInIds(userId).stream()
            .filter(user -> !user.getLogin().equals(securityContext.getCurrentUsername()))
            .map(this::getProfile)
            .peek(this::fillProfile)
            .collect(Collectors.toList());
    }

    public Profile getProfile(@NonNull User user) {
        UserCard userCard = userCardRepository.getByUser(user);
        return new Profile(user, userCard);
    }

    @Override
    public Profile fillProfile(Profile profile) {
        Optional<UserCard> card = userCardRepository.getEntityById(profile.getCard().getId());
        if (card.isEmpty()) {
            log.error("Empty user profile");
            throw new NullPointerException(
                "User card with id " + profile.getCard().getId() + " not found ");
        }

        return new Profile(card.get());
    }

    @Override
    public Collection<Profile> search(Map<String, String> clause, Pageable page) {
        return userCardRepository.search(cleanClause(clause), page).stream()
            .map(Profile::new)
            .collect(Collectors.toList());
    }

    @Override
    public long countSearch(Map<String, String> clause) {
        return userCardRepository.countSearch(clause);
    }

    private Map<String, String> cleanClause(Map<String, String> clause) {
        clause.remove("sort");
        clause.remove("page");
        clause.remove("size");
        return clause;
    }
}
