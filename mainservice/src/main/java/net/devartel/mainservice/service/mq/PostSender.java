package net.devartel.mainservice.service.mq;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.devartel.api.domain.Post;
import net.devartel.mainservice.config.RabbitMQConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

public class PostSender {

    @Autowired
    private RabbitTemplate template;

    public void send(long routeId, Post post) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        this.template.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE_NAME,String.valueOf(routeId), mapper.writeValueAsString(post));
    }
}
