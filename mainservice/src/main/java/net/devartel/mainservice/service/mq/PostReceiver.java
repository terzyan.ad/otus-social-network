package net.devartel.mainservice.service.mq;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.mainservice.config.RabbitMQConfig;
import net.devartel.mainservice.controller.SocketController;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class PostReceiver {

    private final SocketController socketController;

    @RabbitListener(queues = RabbitMQConfig.QUEUE_NAME)
    public void receiveMessage(@Payload String post,
      @Header(AmqpHeaders.RECEIVED_ROUTING_KEY) String key) {
        log.info("Routing key: {}, value {}", key, post);
        socketController.sendToUser(Long.parseLong(key), post);
    }

}
