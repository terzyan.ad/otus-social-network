package net.devartel.mainservice.service.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserCard;
import net.devartel.api.domain.dto.RegistrationCard;
import net.devartel.api.repository.BaseRepository;
import net.devartel.api.service.RegistrationService;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Primary
@Service
public class RegistrationServiceImpl implements RegistrationService {

    private final BaseRepository<User, Long> userRepository;
    private final BaseRepository<UserCard, Long> userCardBaseRepository;
    private final PasswordEncoder encoder;

    public RegistrationServiceImpl(BaseRepository<User, Long> userRepository,
                                   BaseRepository<UserCard, Long> userCardBaseRepository,
                                   PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.userCardBaseRepository = userCardBaseRepository;
        this.encoder = encoder;
    }

    @Override
    public String registration(RegistrationCard card) {

        User user = userRepository.save(new User(card, encoder));

        UserCard userCard = new UserCard(card);
        userCard.setUser(user);
        userCardBaseRepository.save(userCard);

        return "";
    }

    @Override
    public String registration(Collection<RegistrationCard> cards) {
        Collection<User> users = new HashSet<>();
        for(RegistrationCard card:cards)
            users.add(new User(card, encoder));

        users = userRepository.save(users);

        Collection<UserCard> userCards = new HashSet<>();
        Map<String,RegistrationCard> cardMap = cards.stream().collect(
            Collectors.toMap(RegistrationCard::getMail, Function.identity()));
        for(User user: users){
            UserCard userCard = new UserCard(cardMap.get(user.getLogin()));
            userCard.setUser(user);
            userCards.add(userCard);
        }
        userCardBaseRepository.save(userCards);

        return "";
    }
}
