create table users
(
    id       int auto_increment,
    login    varchar(256) not null,
    password varchar(512) not null,
    name    varchar(256) not null,
    constraint users_pk
        primary key (id)
);

create unique index users_login_uindex
    on users (login);


create table user_card
(
    id      int auto_increment,
    user_id int          null,
    mail    varchar(256) not null,
    first_name    varchar(256) null,
    second_name    varchar(256) null,
    sex    varchar(256) null,
    city    varchar(256) null,
    interest    varchar(256) null,
    age    int null,
    constraint user_card_pk
        primary key (id),
    constraint user_card_user_fk
        foreign key (user_id) references users (id)
            on delete cascade
);

create table user_relation
(
    id              int auto_increment,
    request_user_id int not null,
    target_user_id  int not null,
    relation_type   int not null,
    constraint user_relation_pk
        primary key (id),
    constraint user_relation_request_fk
        foreign key (request_user_id) references users (id),
    constraint user_relation_target_fk
        foreign key (target_user_id) references users (id)
);

create table post
(
    id          int auto_increment,
    user_id     int           null,
    title       varchar(128)  null,
    content     varchar(2048) null,
    last_update int           null,
    constraint post_pk
        primary key (id),
    constraint post_user_fk
        foreign key (user_id) references users (id)
);

