-- auto-generated definition
create table chat_users
(
    id             int auto_increment primary key,
    chat_id        int           not null,
    user_id        int           null,
    access_type_id int default 1 not null,
    constraint chat_users_chat_fk
        foreign key (chat_id) references chat_room (id)
);

create index chat_id_users_index
    on chat_users (chat_id);

create index chat_users_id_index
    on chat_users (user_id);

-- auto-generated definition
create table chat_msg
(
    id          int auto_increment primary key,
    chat_id     int           not null,
    user_id     int           not null,
    msg         varchar(2048) null,
    last_update bigint        not null,
    constraint chat_msg_chat_fk
        foreign key (chat_id) references chat_room (id)
            on delete cascade
);

create index chat_msg_chat_index
    on chat_msg (chat_id, user_id, last_update);


