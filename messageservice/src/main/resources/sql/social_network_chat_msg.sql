-- auto-generated definition
create table chat_msg
(
    id          int auto_increment
        primary key,
    chat_id     int           not null,
    user_id     int           not null,
    msg         varchar(2048) null,
    last_update bigint        not null,
    constraint chat_msg_chat_fk
        foreign key (chat_id) references chat_room (id)
            on delete cascade
);

create index chat_msg_chat_index
    on chat_msg (chat_id, user_id, last_update);

