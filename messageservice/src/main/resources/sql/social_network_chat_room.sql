create table chat_room
(
    id   int auto_increment
        primary key,
    name varchar(128) default 'chat' null,
    code varchar(256)                not null,
    constraint chat_room_code_uindex
        unique (code)
);