package net.devartel.messageservice.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final String API_PATH ="/api";
    public static final String API_VERSION = "/v2";

    public static final String API_URL = API_PATH + API_VERSION;
}
