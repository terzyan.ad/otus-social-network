package net.devartel.messageservice.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.UUID;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.ChatRoom;
import net.devartel.api.domain.dto.Chat;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.repository.ChatRoomRepository;
import net.devartel.api.repository.impl.BaseRepositoryImpl;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import net.devartel.messageservice.repository.mapper.ChatMessageMapper;
import net.devartel.messageservice.repository.mapper.ChatRoomMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ChatRoomRepositoryImpl extends BaseRepositoryImpl<ChatRoom, Long> implements ChatRoomRepository {


    private final ChatRoomMapper mapper;

    public ChatRoomRepositoryImpl(DataSource dataSource) {
        super(dataSource);
        this.mapper = new ChatRoomMapper();
    }

    @Override
    public String getTableName() {
        return "chat_room";
    }

    @Override
    public RowMapper<ChatRoom> getRowMapper() {
        return mapper;
    }

    @Override
    public PreparedStatementMapper<ChatRoom> getStatementMapper() {
        return mapper;
    }

    @Override
    public Collection<ChatRoom> getChatByUserId(long userId) {
        String query = "SELECT chat.* FROM " + getTableName() + " as chat "
            + "LEFT JOIN chat_users AS usr ON usr.chat_id=chat.id "
            + "WHERE usr.user_id = ?";
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1,userId);

            ResultSet resultSet = statement.executeQuery();

            List<ChatRoom> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next()) {
                result.add(getRowMapper().mapRow(resultSet, row++));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<ChatRoom> getChatByPairUsersId(long userId, long secondUserId) {
        String query = "SELECT chat.* FROM " + getTableName() + " as chat "
            + "LEFT JOIN chat_users AS usr ON usr.chat_id=chat.id "
            + "LEFT JOIN chat_users AS usr2 ON usr2.chat_id=chat.id "
            + "WHERE usr.user_id = ? AND usr2.user_id = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1,userId);
            statement.setLong(2,secondUserId);

            ResultSet resultSet = statement.executeQuery();

            List<ChatRoom> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next()) {
                result.add(getRowMapper().mapRow(resultSet, row++));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Optional<ChatRoom> getChatByIdAndUserId(long chatId, long userId) {

        String query = "SELECT chat.* FROM " + getTableName() + " as chat "
            + "LEFT JOIN chat_users AS usr ON usr.chat_id=chat.id "
            + "WHERE usr.user_id = ? AND chat.id = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1,userId);
            statement.setLong(2,chatId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.ofNullable(getRowMapper().mapRow(resultSet, 0));
            }

            return Optional.empty();

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    public Optional<ChatRoom> getChatByCode(String code) {

        String query = "SELECT * FROM " + getTableName() + " "
            + "WHERE code = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1,code);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.ofNullable(getRowMapper().mapRow(resultSet, 0));
            }

            return Optional.empty();

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<Long> getUsersIdByChatId(long chatId) {

        String query = "SELECT DISTINCT usr.user_id FROM chat_users AS usr "
            + "WHERE usr.chat_id = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1,chatId);

            ResultSet resultSet = statement.executeQuery();

            Set<Long> result = new HashSet<>();

            while (resultSet.next()) {
                result.add(resultSet.getLong(1));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Chat save(Chat chat) {
        ChatRoom chatRoom = new ChatRoom(null, UUID.randomUUID().toString(), chat.getName());

        chatRoom = getChatByCode(save(chatRoom).getCode())
            .orElseThrow(()->new RuntimeException("Проблема сохранения чата"));

        String query = "INSERT INTO chat_users (" +
            new StringJoiner(",")
                .add(ChatMessageMapper.FIELD_CHAT_ID)
                .add(ChatMessageMapper.FIELD_USER_ID)
            + ") VALUES (?,?)";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {

            for (Profile entity : chat.getUsers()) {
                statement.setLong(1,chatRoom.getId());
                statement.setLong(2,entity.getId());
                statement.addBatch();
            }

            int[] resultSet = statement.executeBatch();
            if (resultSet.length == 0) {
                log.warn("Запрос на записей запроса {} в количестве {} невыполнился",
                    getStatementMapper().getInsertQuery(), chat.getUsers().size());
                throw new RuntimeException("Запрос добавления пользователей в чат вызвал ошибку");
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }

        chat.setId(chatRoom.getId());
        return chat;
    }
}
