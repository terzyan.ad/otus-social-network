package net.devartel.messageservice.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.ChatMessage;
import net.devartel.api.domain.dto.ChatContent;
import net.devartel.api.repository.ChatMessageRepository;
import net.devartel.api.repository.impl.BaseRepositoryImpl;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import net.devartel.messageservice.repository.mapper.ChatMessageMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ChatMessageRepositoryImpl extends BaseRepositoryImpl<ChatMessage, Long> implements ChatMessageRepository {

    private final ChatMessageMapper mapper;

    public ChatMessageRepositoryImpl(DataSource dataSource) {
        super(dataSource);
        this.mapper = new ChatMessageMapper();
    }

    @Override
    public String getTableName() {
        return "chat_msg";
    }

    @Override
    public RowMapper<ChatMessage> getRowMapper() {
        return mapper;
    }

    @Override
    public PreparedStatementMapper<ChatMessage> getStatementMapper() {
        return mapper;
    }

    @Override
    public String getLastMessageByChatId(long chatId) {
        String query = "SELECT * FROM " + getTableName()
            + " WHERE " + ChatMessageMapper.FIELD_CHAT_ID + " =?"
            + " ORDER BY " + ChatMessageMapper.FIELD_LAST_UPDATE + " DESC"
            + " LIMIT 1";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, chatId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return getRowMapper().mapRow(resultSet, 0).getMessage();
            } else {
                return "";
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<ChatMessage> getMessageFromChat(long chatId) {
        String query = "SELECT * FROM " + getTableName()
            + " WHERE " + ChatMessageMapper.FIELD_CHAT_ID + " =?"
            + " ORDER BY " + ChatMessageMapper.FIELD_LAST_UPDATE + " ASC";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, chatId);

            ResultSet resultSet = statement.executeQuery();

            ArrayList<ChatMessage> result = new ArrayList<>();

            int row = 0;
            while (resultSet.next()) {
                result.add(getRowMapper().mapRow(resultSet, row++));
            }
            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public ChatMessage save(ChatContent content) {
        return save(new ChatMessage(content));
    }
}
