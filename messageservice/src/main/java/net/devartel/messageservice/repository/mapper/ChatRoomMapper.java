package net.devartel.messageservice.repository.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import net.devartel.api.domain.ChatRoom;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import org.springframework.jdbc.core.RowMapper;

public class ChatRoomMapper implements RowMapper<ChatRoom>, PreparedStatementMapper<ChatRoom> {

    public static final String TABLE_NAME = "chat_room";

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_CODE = "code";

    @Override
    public ChatRoom mapRow(ResultSet rs, int rowNum) throws SQLException {
        ChatRoom chatRoom = new ChatRoom();

        chatRoom.setId(rs.getLong(FIELD_ID));
        chatRoom.setCode(rs.getString(FIELD_CODE));
        chatRoom.setName(rs.getString(FIELD_NAME));

        return chatRoom;
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + TABLE_NAME + " (" + FIELD_NAME + "," + FIELD_CODE + ") VALUES (?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE  " + TABLE_NAME          + " SET " +
            ChatRoomMapper.FIELD_NAME           + "= ?, " +
            ChatRoomMapper.FIELD_CODE           + "= ? "  +
            "WHERE " + ChatRoomMapper.FIELD_ID  + "= ?";
    }

    @Override
    public PreparedStatement mapStatementInsert(PreparedStatement statement, ChatRoom entity) throws SQLException {
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getCode());
        return statement;
    }

    @Override
    public PreparedStatement mapStatementUpdate(PreparedStatement statement, ChatRoom entity) throws SQLException {
        statement = mapStatementInsert(statement, entity);
        statement.setLong(3, entity.getId());
        return statement;
    }

    @Override
    public String getFullSelectQuery() {
        return  "SELECT * FROM " + TABLE_NAME   + " WHERE " +
            ChatRoomMapper.FIELD_NAME           + "= ? AND " +
            ChatRoomMapper.FIELD_CODE           + "= ? ";
    }
}
