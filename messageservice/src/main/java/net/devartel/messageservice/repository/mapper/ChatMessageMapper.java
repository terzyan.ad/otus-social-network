package net.devartel.messageservice.repository.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;
import net.devartel.api.domain.ChatMessage;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import org.springframework.jdbc.core.RowMapper;

public class ChatMessageMapper implements RowMapper<ChatMessage>, PreparedStatementMapper<ChatMessage> {

    public static final String TABLE_NAME = "chat_msg";

    public static final String FIELD_ID = "id";
    public static final String FIELD_CHAT_ID = "chat_id";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_MESSAGE = "msg";
    public static final String FIELD_LAST_UPDATE = "last_update";

    @Override
    public ChatMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
        ChatMessage chatMessage = new ChatMessage();

        chatMessage.setId(rs.getLong(FIELD_ID));
        chatMessage.setChatId(rs.getLong(FIELD_CHAT_ID));
        chatMessage.setMessage(rs.getString(FIELD_MESSAGE));
        chatMessage.setUserId(rs.getLong(FIELD_USER_ID));
        chatMessage.setLastUpdate(rs.getLong(FIELD_LAST_UPDATE));

        return chatMessage;
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + TABLE_NAME + " (" +
            new StringJoiner(",")
                .add(ChatMessageMapper.FIELD_CHAT_ID)
                .add(ChatMessageMapper.FIELD_USER_ID)
                .add(ChatMessageMapper.FIELD_MESSAGE)
                .add(ChatMessageMapper.FIELD_LAST_UPDATE)
            + ") VALUES (?,?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE  " + TABLE_NAME              + " SET " +
            ChatMessageMapper.FIELD_CHAT_ID         + "= ?, " +
            ChatMessageMapper.FIELD_USER_ID         + "= ?, " +
            ChatMessageMapper.FIELD_MESSAGE         + "= ?, " +
            ChatMessageMapper.FIELD_LAST_UPDATE     + "= ? "  +
            "WHERE " + ChatMessageMapper.FIELD_ID   + "= ?";
    }

    @Override
    public String getFullSelectQuery() {
        return  "SELECT * FROM " + TABLE_NAME       + " WHERE " +
            ChatMessageMapper.FIELD_CHAT_ID         + "= ? AND " +
            ChatMessageMapper.FIELD_USER_ID         + "= ? AND " +
            ChatMessageMapper.FIELD_MESSAGE         + "= ? AND " +
            ChatMessageMapper.FIELD_LAST_UPDATE     + "= ? ";
    }

    @Override
    public PreparedStatement mapStatementInsert(PreparedStatement statement, ChatMessage entity) throws SQLException {
        statement.setLong(1, entity.getChatId());
        statement.setLong(2, entity.getUserId());
        statement.setString(3, entity.getMessage());
        statement.setLong(4, System.currentTimeMillis() / 1000);
        return statement;
    }

    @Override
    public PreparedStatement mapStatementUpdate(PreparedStatement statement, ChatMessage entity) throws SQLException {
        statement = mapStatementInsert(statement, entity);
        statement.setLong(5, entity.getId());
        return statement;
    }
}
