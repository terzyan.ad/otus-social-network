package net.devartel.messageservice.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.ChatRoom;
import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.dto.Chat;
import net.devartel.api.domain.dto.ChatContent;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.repository.ChatMessageRepository;
import net.devartel.api.repository.ChatRoomRepository;
import net.devartel.api.service.ChatService;
import net.devartel.api.service.FriendshipService;
import net.devartel.api.service.ProfileService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Primary
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {

    private final ProfileService profileService;

    private final FriendshipService friendshipService;

    private final ChatRoomRepository chatRoomRepository;

    private final ChatMessageRepository chatMessageRepository;

    @Override
    public Collection<Chat> getAllChats() {
        Profile profile = profileService.currentProfile();

        log.info("Request chat list for user {}", profile.getId());

        Collection<ChatRoom> chats = chatRoomRepository.getChatByUserId(profile.getId());
        return chats.stream()
            .map(Chat::new)
            .peek(chat -> chat.setUsers(
                profileService.getProfilesByUserId(chatRoomRepository.getUsersIdByChatId(chat.getId()))))
            .peek(chat -> chat.setLastMessage(chatMessageRepository.getLastMessageByChatId(chat.getId())))
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Chat> getChatById(long id) {
        Profile profile = profileService.currentProfile();
        Optional<ChatRoom> chatRoom = chatRoomRepository.getChatByIdAndUserId(id, profile.getId());

        if (chatRoom.isEmpty()) {
            return Optional.empty();
        }

        log.info("Request chat by id {}", chatRoom.get().getId());

        Chat chat = new Chat(chatRoom.get());
        chat.setUsers(profileService.getProfilesByUserId(chatRoomRepository.getUsersIdByChatId(chat.getId())));
        chat.setLastMessage(chatMessageRepository.getLastMessageByChatId(chat.getId()));
        chat.setMessages(chatMessageRepository.getMessageFromChat(chat.getId()).stream()
            .map(chatMessage -> new ChatContent(chatMessage, chat.getMapUsers()))
            .collect(Collectors.toList()));
        return Optional.of(chat);
    }

    @Override
    public Collection<Chat> getChatByUserId(long userId) {
        Profile profile = profileService.currentProfile();
        Collection<ChatRoom> chats = chatRoomRepository.getChatByPairUsersId(profile.getId(), userId);
        if (chats.isEmpty()) {
            log.info("Init new chat for users {}, {}", userId, profile.getId());
            return createNewChat(profileService.getProfilesByUserId(Arrays.asList(profile.getId(), userId)));
        }
        log.info("Search chat for users {}, {}", userId, profile.getId());
        return chats.stream()
            .map(Chat::new)
            .peek(chat -> chat.setUsers(
                profileService.getProfilesByUserId(chatRoomRepository.getUsersIdByChatId(chat.getId()))))
            .peek(chat -> chat.setLastMessage(chatMessageRepository.getLastMessageByChatId(chat.getId())))
            .collect(Collectors.toList());
    }

    @Override
    public Collection<Chat> search(String name) {
        Collection<Chat> chats = getAllChats().stream()
            .filter(chat -> chat.getName().toLowerCase(Locale.ROOT).contains(name.toLowerCase(Locale.ROOT)))
            .collect(Collectors.toSet());

        Collection<Profile> friendChat = chats.stream()
            .filter(chat -> chat.getUsers().size() == 1)
            .map(chat -> chat.getUsers().iterator().next())
            .collect(Collectors.toSet());

        Collection<Profile> friends = friendshipService.getUserByRelationType(RelationType.FRIEND).stream()
            .filter(profile -> !friendChat.contains(profile))
            .filter(profile ->
                profile.getCard().getFirstName().toLowerCase(Locale.ROOT).contains(name.toLowerCase(Locale.ROOT)) ||
                profile.getCard().getSecondName().toLowerCase(Locale.ROOT).contains(name.toLowerCase(Locale.ROOT)))
            .collect(Collectors.toSet());

        chats.addAll(toChatCandidate(friends));

        return chats;
    }

    @Override
    public void receiveContent(ChatContent content) {
        Profile profile = profileService.currentProfile();
        Optional<ChatRoom> chat = chatRoomRepository.getChatByIdAndUserId(content.getChatId(), profile.getId());

        if (chat.isEmpty()) {
            log.error("Failed send message to chat {}", content.getChatId());
            throw new RuntimeException("Chat unavailable");
        }

        content.setUser(profileService.currentProfile());
        content.setLastUpdate(System.currentTimeMillis());
        chatMessageRepository.save(content);
        log.info("Message send to chat {}", content.getChatId());
    }

    private Collection<Chat> createNewChat(Collection<Profile> users) {

        users.add(profileService.currentProfile());

        ChatRoom chatRoom = new ChatRoom();
        chatRoom.setName(usersToDialogName(users));

        Chat newChat = new Chat(chatRoom);
        newChat.setUsers(users);

        return Collections.singleton(chatRoomRepository.save(newChat));
    }

    private String usersToDialogName(Collection<Profile> users) {
        return users.stream().map(u -> u.getCard().getFirstName() + " " + u.getCard().getSecondName())
            .collect(Collectors.joining(", "));
    }

    private Collection<Chat> toChatCandidate(Collection<Profile> profiles) {
        return profiles.stream()
            .map(Chat::new)
            .collect(Collectors.toSet());
    }
}
