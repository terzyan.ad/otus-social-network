package net.devartel.api.domain;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.devartel.api.domain.dto.Profile;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatRoom implements DbEntity<Long> {

    private Long id;
    private String code;
    private String name;

    public ChatRoom(Profile profile) {
        this.id = 0L;
        this.code = UUID.randomUUID().toString();
        this.name = profile.getCard().getFirstName() + " " + profile.getCard().getSecondName();
    }
}
