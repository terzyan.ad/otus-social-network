package net.devartel.api.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserCard;

@Data
@NoArgsConstructor
public class Profile {

    private Long id;
    private String userLogin;
    private UserCard card;


    public Profile(UserCard card) {
        this.userLogin = card.getMail();
        this.card = card;
    }

    public Profile(User user, UserCard card) {
        this.id = user.getId();
        this.userLogin = user.getLogin();
        this.card = card;
    }
}
