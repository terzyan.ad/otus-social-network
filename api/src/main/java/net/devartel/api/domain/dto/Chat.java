package net.devartel.api.domain.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.devartel.api.domain.ChatRoom;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Chat {

    private Long id;
    private String name;
    private Collection<Profile> users;
    private String lastMessage;
    private Collection<ChatContent> messages = new ArrayList<>();

    public Chat(ChatRoom chatRoom){
        super();
        this.id = chatRoom.getId();
        this.name = chatRoom.getName();
    }

    public Chat(Profile user){
        this(new ChatRoom(user));
        users = Collections.singletonList(user);
    }

    public Map<Long, Profile> getMapUsers(){
        return users.stream().collect(Collectors.toMap(Profile::getId, Function.identity()));
    }

}
