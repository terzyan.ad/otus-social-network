package net.devartel.api.domain;

import java.util.Arrays;

public enum RelationType {
    CANDIDATE(0),
    FRIEND(1),
    BLOCKED(2);

    private final int type;

    RelationType(int type) {
        this.type = type;
    }

    public static RelationType valueOfLabel(int type) {
        return Arrays.stream(values())
                .filter(relation -> relation.type == type)
                .findFirst()
                .orElse(null);
    }
}
