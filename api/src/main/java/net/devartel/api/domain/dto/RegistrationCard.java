package net.devartel.api.domain.dto;

import lombok.Data;

@Data
public class RegistrationCard {
    private String mail;
    private String password;
    private String firstName;
    private String secondName;
    private String sex;
    private short age;
    private String city;
    private String interest;
}
