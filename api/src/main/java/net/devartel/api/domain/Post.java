package net.devartel.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.devartel.api.domain.dto.PostContent;
import net.devartel.api.domain.dto.Profile;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Post implements DbEntity<Long> {
    private Long id;
    private long userId;
    private String title;
    private String content;
    private long lastUpdate;
    private Profile user;

    public Post(PostContent postContent){
        this.title = postContent.getTitle();
        this.content = postContent.getContent();
        this.lastUpdate = System.currentTimeMillis();
    }

    public void setProfile(Profile user){
        this.setUserId(user.getCard().getId());
        this.user = user;
    }

}
