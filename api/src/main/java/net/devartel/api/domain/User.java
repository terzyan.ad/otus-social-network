package net.devartel.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.devartel.api.domain.dto.RegistrationCard;
import org.springframework.security.crypto.password.PasswordEncoder;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements DbEntity<Long> {
    private Long id;
    private String login;
    private String password;
    private String name;

    //TODO: выделить интерфейс и создать прокси для ленивого заполнения данных по пользователю
    public User(Long id){
        this.id = id;
    }

    public User(RegistrationCard registrationCard, PasswordEncoder encoder){
        this.login = registrationCard.getMail();
        this.name = registrationCard.getFirstName();
        this.password = encoder.encode(registrationCard.getPassword());
    }
}
