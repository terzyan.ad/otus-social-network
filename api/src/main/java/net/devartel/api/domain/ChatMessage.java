package net.devartel.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.devartel.api.domain.dto.ChatContent;
import net.devartel.api.domain.dto.Profile;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessage implements DbEntity<Long> {
    private Long id;
    private long chatId;
    private long userId;
    private String message;
    private long lastUpdate;
    private Profile user;

    public ChatMessage(ChatContent content){
        this.chatId = content.getChatId();
        this.message = content.getMsg();
        this.userId = content.getUser().getId();
        this.user = content.getUser();
    }
}
