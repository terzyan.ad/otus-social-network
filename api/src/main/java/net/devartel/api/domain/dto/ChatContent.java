package net.devartel.api.domain.dto;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.devartel.api.domain.ChatMessage;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatContent {

    private Long chatId;
    private Profile user;
    private String msg;
    private long lastUpdate;

    public ChatContent(ChatMessage ms, Map<Long, Profile> users) {
        this.chatId = ms.getChatId();
        this.user = users.get(ms.getUserId());
        this.msg = ms.getMessage();
        this.lastUpdate = ms.getLastUpdate();
    }
}
