package net.devartel.api.domain;

import lombok.Data;

@Data
public class UserRelation implements DbEntity<Long> {
    private Long id;
    private User requestUser;
    private User targetUser;
    private RelationType relationType;
}
