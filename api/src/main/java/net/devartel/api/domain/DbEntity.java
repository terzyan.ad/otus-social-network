package net.devartel.api.domain;

import java.io.Serializable;

public interface DbEntity<ID extends Serializable> {

    ID getId();
}
