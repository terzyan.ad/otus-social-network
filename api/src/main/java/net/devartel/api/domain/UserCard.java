package net.devartel.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.devartel.api.domain.dto.RegistrationCard;

@Getter
@Setter
@NoArgsConstructor
public class UserCard implements DbEntity<Long> {
    private Long id;
    @JsonIgnore
    private transient User user;
    private String mail;
    private String firstName;
    private String secondName;
    private String sex;
    private short age;
    private String city;
    private String interest;


    public UserCard(RegistrationCard card){
        this.mail = card.getMail();
        this.firstName = card.getFirstName();
        this.secondName = card.getSecondName();
        this.sex = card.getSex();
        this.age = card.getAge();
        this.city = card.getCity();
        this.interest = card.getInterest();
    }
}
