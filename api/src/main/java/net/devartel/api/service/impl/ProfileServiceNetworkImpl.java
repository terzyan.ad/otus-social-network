package net.devartel.api.service.impl;

import java.util.Collection;
import java.util.Map;
import java.util.StringJoiner;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.config.ServiceConfig;
import net.devartel.api.domain.User;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.service.ProfileService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class ProfileServiceNetworkImpl extends AbstractNetworkService implements ProfileService {

    public ProfileServiceNetworkImpl(ServiceConfig serviceConfig) {
        super(serviceConfig);
    }

    public String getPath() {
        return "/profile/";
    }

    @Override
    public Profile currentProfile() {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("profile") + getPath(),
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            Profile.class).getBody();
    }

    @Override
    public Collection<Profile> getAllProfiles() {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("profile") + getPath() + "all",
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Profile>>() {}).getBody();
    }

    @Override
    public Collection<Profile> search(Map<String, String> clause, Pageable page) {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            createUrl(clause, "all"),
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Profile>>() {}).getBody();
    }

    @Override
    public long countSearch(Map<String, String> clause) {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            createUrl(clause, "count_search"),
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            Long.class).getBody();
    }

    @Override
    public Collection<Profile> getProfilesByUserId(Collection<Long> userId) {
        RestTemplate template = new RestTemplate();

        String url = getUrl("profile") + getPath() + "id/";
        if (!userId.isEmpty()) {
            StringJoiner params = new StringJoiner(",");
            userId.stream().forEach(id -> params.add(String.valueOf(id)));
            url += params;
        }

        return template.exchange(
            url,
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Profile>>() {}).getBody();
    }

    @Override
    public Profile getProfile(User user) {
        return null;
    }

    @Override
    public Profile fillProfile(Profile profile) {
        return null;
    }

    private String createUrl(Map<String, String> clause, String path) {

        String url = getUrl("profile") + getPath() + path;

        if (!clause.isEmpty()) {
            StringJoiner clauseString = new StringJoiner("&");
            for (String key : clause.keySet()) {
                clauseString.add(key + "=" + clause.get(key));
            }
            url += "?" + clauseString;
        }
        return url;
    }

}
