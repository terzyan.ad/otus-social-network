package net.devartel.api.service.impl;

import java.util.Collection;
import java.util.Optional;
import net.devartel.api.config.ServiceConfig;
import net.devartel.api.domain.dto.Chat;
import net.devartel.api.domain.dto.ChatContent;
import net.devartel.api.service.ChatService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ChatServiceNetworkImpl extends AbstractNetworkService implements ChatService {

    public ChatServiceNetworkImpl(ServiceConfig serviceConfig) {
        super(serviceConfig);
    }

    public String getPath() {
        return "/chat/";
    }

    @Override
    public Collection<Chat> getAllChats() {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("message") + getPath(),
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Chat>>() {}).getBody();
    }

    @Override
    public Optional<Chat> getChatById(long id) {
        RestTemplate template = new RestTemplate();
        return Optional.ofNullable(template.exchange(
            getUrl("message") + getPath() + id,
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            Chat.class).getBody());
    }

    @Override
    public Collection<Chat> getChatByUserId(long userId) {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("message") + getPath() + "user/" + userId,
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Chat>>() {}).getBody();
    }

    @Override
    public Collection<Chat> search(String name) {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("message") + getPath() + "search/" + name,
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Chat>>() {}).getBody();
    }

    @Override
    public void receiveContent(ChatContent content) {
        RestTemplate template = new RestTemplate();
        HttpEntity<ChatContent> request = new HttpEntity<>(content, getHeaders());
        template.postForEntity(getUrl("message") + getPath(), request, String.class);
    }
}
