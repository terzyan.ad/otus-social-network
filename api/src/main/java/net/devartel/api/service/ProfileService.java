package net.devartel.api.service;

import java.util.Map;
import net.devartel.api.domain.User;
import net.devartel.api.domain.dto.Profile;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.NonNull;

import java.util.Collection;

public interface ProfileService {

    Profile currentProfile();

    Collection<Profile> getAllProfiles();

    Collection<Profile> getProfilesByUserId(Collection<Long> userId);

    Profile getProfile(@NonNull User user);

    Profile fillProfile(Profile profile);

    Collection<Profile> search(Map<String,String> clause, Pageable page);

    long countSearch(Map<String,String> clause);
}
