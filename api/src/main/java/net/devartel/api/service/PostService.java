package net.devartel.api.service;

import java.util.Collection;
import java.util.Optional;
import net.devartel.api.domain.Post;
import net.devartel.api.domain.dto.PostContent;
import org.springframework.data.domain.Pageable;

public interface PostService {

    Collection<Post> getAllPosts(Pageable page);

    Optional<Post> getPostById(long id);

    Collection<Post> getPostByUserId(long userId, Pageable page);

    void receiveContent(PostContent content);

}
