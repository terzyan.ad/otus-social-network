package net.devartel.api.service.impl;

import java.util.Collection;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.config.ServiceConfig;
import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.dto.Profile;
import net.devartel.api.service.FriendshipService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class FriendshipServiceNetworkImpl extends AbstractNetworkService implements FriendshipService {

    public FriendshipServiceNetworkImpl(ServiceConfig serviceConfig) {
        super(serviceConfig);
    }

    public String getPath() {
        return "/friend/";
    }

    @Override
    public void requestToFriendship(Profile profile) {
        RestTemplate template = new RestTemplate();
        HttpEntity<Profile> request = new HttpEntity<>(profile, getHeaders());
        template.postForEntity(getUrl("friendship") + getPath(), request, String.class);
    }

    @Override
    public void acceptFriendship(Profile profile) {
        RestTemplate template = new RestTemplate();
        HttpEntity<Profile> request = new HttpEntity<>(profile, getHeaders());
        template.postForEntity(getUrl("friendship") + getPath() + "accept", request, String.class);
    }

    @Override
    public void rejectFriendship(Profile profile) {
    }

    @Override
    public void blockUser(Profile profile) {
        RestTemplate template = new RestTemplate();
        HttpEntity<Profile> request = new HttpEntity<>(profile, getHeaders());
        template.postForEntity(getUrl("friendship") + getPath() + "block/", request, String.class);
    }

    @Override
    public void unblockUser(Profile profile) {
        RestTemplate template = new RestTemplate();
        HttpEntity<Profile> request = new HttpEntity<>(profile, getHeaders());
        template.postForEntity(getUrl("friendship") + getPath() + "unblock/", request, String.class);
    }

    @Override
    public Collection<Profile> getUserByRelationType(RelationType type) {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("friendship") + getPath() + "by-type/" + type.name(),
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Profile>>() {}).getBody();
    }

    @Override
    public Collection<Profile> getSubscribed() {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("friendship") + getPath() + "subscribed/",
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Profile>>() {}).getBody();
    }

    @Override
    public Collection<Profile> getAll() {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("friendship") + getPath() + "all/",
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Profile>>() {}).getBody();
    }

}
