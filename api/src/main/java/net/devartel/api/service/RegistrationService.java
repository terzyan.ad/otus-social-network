package net.devartel.api.service;

import java.util.Collection;
import net.devartel.api.domain.dto.RegistrationCard;

public interface RegistrationService {
    String registration(RegistrationCard card);
    String registration(Collection<RegistrationCard> cards);
}
