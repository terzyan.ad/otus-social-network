package net.devartel.api.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.config.ServiceConfig;
import net.devartel.api.domain.Post;
import net.devartel.api.domain.dto.PostContent;
import net.devartel.api.service.PostService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class PostServiceNetworkImpl extends AbstractNetworkService implements PostService {

    public PostServiceNetworkImpl(ServiceConfig serviceConfig) {
        super(serviceConfig);
    }

    public String getPath() {
        return "/post/";
    }

    @Override
    public Collection<Post> getAllPosts(Pageable page) {
        RestTemplate template = new RestTemplate();
        return template.exchange(
            getUrl("post") + getPath(),
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            new ParameterizedTypeReference<Collection<Post>>() {}).getBody();
    }

    @Override
    public Optional<Post> getPostById(long id) {
        RestTemplate template = new RestTemplate();
        return Optional.ofNullable(template.exchange(
            getUrl("post") + getPath() + id,
            HttpMethod.GET,
            new HttpEntity<>(getHeaders()),
            Post.class).getBody());
    }

    @Override
    public Collection<Post> getPostByUserId(long userId, Pageable page) {
        return new ArrayList<>();
    }

    @Override
    public void receiveContent(PostContent content) {
        RestTemplate template = new RestTemplate();
        HttpEntity<PostContent> request = new HttpEntity<>(content, getHeaders());
        template.postForEntity(getUrl("post") + getPath(), request, String.class);
    }

}
