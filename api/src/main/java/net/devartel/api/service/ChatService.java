package net.devartel.api.service;

import java.util.Collection;
import java.util.Optional;
import net.devartel.api.domain.dto.Chat;
import net.devartel.api.domain.dto.ChatContent;

public interface ChatService {

    Collection<Chat> getAllChats();

    Optional<Chat> getChatById(long id);

    Collection<Chat> getChatByUserId(long userId);

    Collection<Chat> search(String name);

    void receiveContent(ChatContent content);

}
