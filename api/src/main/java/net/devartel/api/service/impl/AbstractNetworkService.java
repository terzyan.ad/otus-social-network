package net.devartel.api.service.impl;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import net.devartel.api.config.ServiceConfig;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@RequiredArgsConstructor
public abstract class AbstractNetworkService {

    private final ServiceConfig serviceConfig;

    public abstract String getPath();

    public HttpHeaders getHeaders() {
        String basicAuth = Optional.ofNullable(RequestContextHolder.getRequestAttributes())
            .filter(ServletRequestAttributes.class::isInstance)
            .map(ServletRequestAttributes.class::cast)
            .map(ServletRequestAttributes::getRequest)
            .get().getHeader("Authorization");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", basicAuth);
        return headers;
    }

    public String getUrl(String service) {
        return serviceConfig.getService().getOrDefault(service, serviceConfig.getService().get("all"));
    }

}
