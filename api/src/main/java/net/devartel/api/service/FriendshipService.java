package net.devartel.api.service;

import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.dto.Profile;

import java.util.Collection;

public interface FriendshipService {

    void requestToFriendship(Profile profile);

    void acceptFriendship(Profile profile);

    void rejectFriendship(Profile profile);

    void blockUser(Profile profile);

    void unblockUser(Profile profile);

    Collection<Profile> getUserByRelationType(RelationType type);

    Collection<Profile> getSubscribed();

    Collection<Profile> getAll();
}
