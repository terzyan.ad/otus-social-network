package net.devartel.api.repository;

import java.util.Collection;
import java.util.Map;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserCard;
import org.springframework.data.domain.Pageable;

public interface UserCardRepository extends BaseRepository<UserCard, Long> {
    UserCard getByUser(User user);
    Collection<UserCard> search(Map<String,String> clause, Pageable page);
    long countSearch(Map<String,String> clause);
}
