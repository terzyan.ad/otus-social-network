package net.devartel.api.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

public interface BaseRepository<T, ID extends Serializable> {

    String FIELD_ID = "id";

    Optional<T> getEntityById(ID id);

    Collection<T> getEntityByInIds(Collection<ID> ids);

    Collection<T> getAll();

    T save(T entity);

    Collection<T> save(Collection<T> entities);

    void remove(T entity);
}
