package net.devartel.api.repository;

import java.util.Collection;
import net.devartel.api.domain.ChatMessage;
import net.devartel.api.domain.dto.ChatContent;

public interface ChatMessageRepository extends BaseRepository<ChatMessage, Long> {

    String getLastMessageByChatId(long chatId);

    Collection<ChatMessage> getMessageFromChat(long chatId);

    ChatMessage save(ChatContent content);

}
