package net.devartel.api.repository;

import java.util.Collection;
import java.util.Optional;
import net.devartel.api.domain.ChatRoom;
import net.devartel.api.domain.dto.Chat;

public interface ChatRoomRepository extends BaseRepository<ChatRoom, Long> {

    Collection<ChatRoom> getChatByUserId(long userId);

    Collection<ChatRoom> getChatByPairUsersId(long userId, long secondUserId);

    Optional<ChatRoom> getChatByIdAndUserId(long chatId, long userId);

    Collection<Long> getUsersIdByChatId(long chatId);

    Chat save(Chat chat);

}
