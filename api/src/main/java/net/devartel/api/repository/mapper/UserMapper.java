package net.devartel.api.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import net.devartel.api.domain.User;
import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<User> {

    public static final String FIELD_ID = "id";
    public static final String FIELD_LOGIN = "login";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_PASSWORD = "password";

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();

        user.setId(rs.getLong(FIELD_ID));
        user.setLogin(rs.getString(FIELD_LOGIN));
        user.setName(rs.getString(FIELD_NAME));
        user.setPassword(rs.getString(FIELD_PASSWORD));

        return user;
    }
}
