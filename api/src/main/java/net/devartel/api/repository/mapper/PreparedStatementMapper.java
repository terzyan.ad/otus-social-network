package net.devartel.api.repository.mapper;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface PreparedStatementMapper<T> {

    String getInsertQuery();

    String getUpdateQuery();

    String getFullSelectQuery();

    PreparedStatement mapStatementInsert(PreparedStatement statement, T entity) throws SQLException;

    PreparedStatement mapStatementUpdate(PreparedStatement statement, T entity) throws SQLException;

}
