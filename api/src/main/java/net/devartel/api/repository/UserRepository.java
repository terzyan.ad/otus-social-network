package net.devartel.api.repository;

import net.devartel.api.domain.User;

public interface UserRepository extends BaseRepository<User, Long> {
    User getByLogin(String login);
}
