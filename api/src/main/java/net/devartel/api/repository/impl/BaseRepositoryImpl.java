package net.devartel.api.repository.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.DbEntity;
import net.devartel.api.repository.BaseRepository;
import net.devartel.api.repository.mapper.PreparedStatementMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public abstract class BaseRepositoryImpl<T extends DbEntity<ID>, ID extends Serializable> implements
    BaseRepository<T, ID> {

    protected final DataSource dataSource;

    public abstract String getTableName();

    public abstract RowMapper<T> getRowMapper();

    public abstract PreparedStatementMapper<T> getStatementMapper();

    @Override
    public Optional<T> getEntityById(ID param) {
        String query = "SELECT * FROM " + getTableName() + " WHERE " + BaseRepository.FIELD_ID + " = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            setNeededParam(statement, 1, param);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.ofNullable(getRowMapper().mapRow(resultSet, 0));
            } else {
                return Optional.empty();
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            return Optional.empty();
        }
    }

    @Override
    public Collection<T> getEntityByInIds(Collection<ID> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        String query = "SELECT * FROM " + getTableName() + " WHERE " + BaseRepository.FIELD_ID + " IN  (?"
            + ", ?".repeat(ids.size() - 1) + ")";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            int i = 1;
            for (ID id : ids) {
                setNeededParam(statement, i++, id);
            }

            ResultSet resultSet = statement.executeQuery();

            List<T> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next()) {
                result.add(getRowMapper().mapRow(resultSet, row++));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<T> getAll() {
        String query = "SELECT * FROM " + getTableName();
        Collection<T> result = new HashSet<>();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                result.add(getRowMapper().mapRow(resultSet, resultSet.getRow()));
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            result = new HashSet<>();
        }
        return result;
    }

    @Override
    public T save(T entity) {
        if (entity.getId() == null) {
            return insert(Collections.singleton(entity)).iterator().next();
        } else {
            return update(Collections.singleton(entity)).iterator().next();
        }
    }

    @Override
    public Collection<T> save(Collection<T> entities) {

        Collection<T> toInsert = new HashSet<>();
        Collection<T> toUpdate = new HashSet<>();
        for (T entity : entities) {
            if (Objects.isNull(entity.getId())) {
                toInsert.add(entity);
            } else {
                toUpdate.add(entity);
            }
        }

        Collection<T> result = new ArrayList<>();
        result.addAll(insert(toInsert));
        result.addAll(update(toUpdate));

        return result;
    }

    @Override
    public void remove(T entity) {
        String query = "DELETE FROM " + getTableName() + " WHERE " + BaseRepository.FIELD_ID + "= ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            setNeededParam(statement, 1, entity.getId());

            ResultSet resultSet = statement.executeQuery();
            if (resultSet == null) {
                log.warn("Failed query for delete from table {} by condition={}", getTableName(),
                    entity.getId());
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
        }
    }

    protected void setNeededParam(PreparedStatement statement, int index, Object value) throws SQLException {
        if (value instanceof Short) {
            statement.setShort(index, (Short) value);
        } else if (value instanceof Integer) {
            statement.setInt(index, (Integer) value);
        } else if (value instanceof Long) {
            statement.setLong(index, (Long) value);
        } else if (value instanceof Double) {
            statement.setDouble(index, (Double) value);
        } else if (value instanceof Date) {
            statement.setDate(index, (Date) value);
        } else if (value instanceof String) {
            statement.setString(index, (String) value);
        } else {
            statement.setString(index, String.valueOf(value));
        }
    }

    private Collection<T> insert(Collection<T> entities) {

        if (entities.isEmpty()) {
            return entities;
        }

        String query = getStatementMapper().getInsertQuery();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {

            for (T entity : entities) {
                getStatementMapper().mapStatementInsert(statement, entity);
                statement.addBatch();
            }

            int[] resultSet = statement.executeBatch();
            if (resultSet.length == 0) {
                log.warn("Failed query {} with size {}",
                    getStatementMapper().getInsertQuery(), entities.size());
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }

        return updateEntityFromDB(entities);
    }


    private Collection<T> update(Collection<T> entities) {

        if (entities.isEmpty()) {
            return entities;
        }

        String query = getStatementMapper().getUpdateQuery();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {

            for (T entity : entities) {
                getStatementMapper().mapStatementUpdate(statement, entity);
                statement.addBatch();
            }

            int[] resultSet = statement.executeBatch();
            if (resultSet.length == 0) {
                log.warn("Failed query for update with size {}", entities.size());
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }

        return entities;
    }

    private Collection<T> updateEntityFromDB(Collection<T> entities) {
        Collection<T> result = new ArrayList<>();

        String query = getStatementMapper().getFullSelectQuery();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {

            for (T entity : entities) {
                getStatementMapper().mapStatementInsert(statement, entity);
                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    result.add(getRowMapper().mapRow(resultSet, resultSet.getRow()));
                }
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }

        return result;
    }

}
