package net.devartel.api.repository;

import java.util.Collection;
import net.devartel.api.domain.Post;
import org.springframework.data.domain.Pageable;

public interface PostRepository extends BaseRepository<Post, Long> {
    Collection<Post> getPostsByUserIdIn(Collection<Long> ids, Pageable page);
    Collection<Post> getAll(Collection<Long> ids, Pageable page);
}
