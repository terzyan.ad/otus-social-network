package net.devartel.api.repository;

import net.devartel.api.domain.RelationType;
import net.devartel.api.domain.User;
import net.devartel.api.domain.UserRelation;

import java.util.Collection;

public interface UserRelationRepository extends BaseRepository<UserRelation, Long> {

    Collection<UserRelation> getRelationByUserAndTypes(User user, RelationType... types);

    Collection<UserRelation> getRelationByTargetUser(User requester, User target);

}
