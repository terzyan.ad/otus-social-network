package net.devartel.api.repository.impl;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devartel.api.domain.User;
import net.devartel.api.repository.BaseRepository;
import net.devartel.api.repository.UserRepository;
import net.devartel.api.repository.mapper.UserMapper;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private static final String TABLE_NAME = "users";

    private final DataSource dataSource;

    @Override
    public Optional<User> getEntityById(Long aLong) {
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + BaseRepository.FIELD_ID + " = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, aLong);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.ofNullable(new UserMapper().mapRow(resultSet, 0));
            } else {
                return Optional.empty();
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            return Optional.empty();
        }
    }

    @Override
    public Collection<User> getEntityByInIds(Collection<Long> ids) {

        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + BaseRepository.FIELD_ID + " IN  (?"
            + ", ?".repeat(ids.size() - 1) + ")";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            int i = 1;
            for (long id : ids) {
                statement.setLong(i++, id);
            }

            ResultSet resultSet = statement.executeQuery();

            List<User> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next()) {
                result.add(new UserMapper().mapRow(resultSet, row++));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<User> getAll() {
        String query = "SELECT * FROM " + TABLE_NAME;
        Collection<User> result = new HashSet<>();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                result.add(new UserMapper().mapRow(resultSet, resultSet.getRow()));
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            result = new HashSet<>();
        }
        return result;
    }

    @Override
    public User save(User entity) {
        if (entity.getId() == null) {

            String query = "INSERT INTO " + TABLE_NAME + " (" +
                new StringJoiner(",")
                    .add(UserMapper.FIELD_LOGIN)
                    .add(UserMapper.FIELD_PASSWORD)
                    .add(UserMapper.FIELD_NAME)
                + ") VALUES (?,?,?)";

            try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, entity.getLogin());
                statement.setString(2, entity.getPassword());
                statement.setString(3, new String(entity.getName().getBytes(), StandardCharsets.UTF_8));

                long resultSet = statement.executeUpdate();
                if (resultSet == 0) {
                    log.warn("Запрос на создание пользователя {} невыполнился", entity.getLogin());
                }
                connection.commit();
            } catch (SQLException ex) {
                log.error(ex.getLocalizedMessage());
                throw new RuntimeException(ex);
            }
        } else {

            String query = "UPDATE  " + TABLE_NAME + " SET " +
                UserMapper.FIELD_PASSWORD + "= ?, " +
                UserMapper.FIELD_NAME + "= ? " +
                "WHERE " + BaseRepository.FIELD_ID + "= ?";

            try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, entity.getPassword());
                statement.setString(2, entity.getName());
                statement.setLong(3, entity.getId());

                ResultSet resultSet = statement.executeQuery();
                if (resultSet == null) {
                    log.warn("Запрос на обновление пользователя {} невыполнился", entity.getLogin());
                }
                connection.commit();
            } catch (SQLException ex) {
                log.error(ex.getLocalizedMessage());
            }

        }

        return getByLogin(entity.getLogin());
    }

    @Override
    public Collection<User> save(Collection<User> entities) {
        Collection<User> toInsert = new HashSet<>();
        Collection<User> toUpdate = new HashSet<>();
        for (User user : entities) {
            if (Objects.isNull(user.getId())) {
                toInsert.add(user);
            } else {
                toUpdate.add(user);
            }
        }

        Collection<User> result = new ArrayList<>();
        result.addAll(insert(toInsert));
        result.addAll(update(toUpdate));

        return result;
    }

    @Override
    public void remove(User entity) {
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + UserMapper.FIELD_ID + "= ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, entity.getId());

            ResultSet resultSet = statement.executeQuery();
            if (resultSet == null) {
                log.warn("Запрос на удаление пользователя {} невыполнился", entity.getLogin());
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
        }
    }

    @Override
    public User getByLogin(String login) {

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + UserMapper.FIELD_LOGIN + " = ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return new UserMapper().mapRow(resultSet, 0);
            } else {
                return null;
            }

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    public Collection<User> getByLogins(Collection<String> logins) {

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + UserMapper.FIELD_LOGIN + " in (?"
            + ",?".repeat(Math.max(0, logins.size() - 1)) + ")";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {
            int i = 1;
            for (String login : logins) {
                statement.setString(i++, login);
            }

            ResultSet resultSet = statement.executeQuery();

            List<User> result = new ArrayList<>();
            int row = 0;
            while (resultSet.next()) {
                result.add(new UserMapper().mapRow(resultSet, row++));
            }

            return result;

        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
    }

    private Collection<User> insert(Collection<User> users) {

        if (users.isEmpty()) {
            return users;
        }

        String query = "INSERT INTO " + TABLE_NAME + " (" +
            new StringJoiner(",")
                .add(UserMapper.FIELD_LOGIN)
                .add(UserMapper.FIELD_PASSWORD)
                .add(UserMapper.FIELD_NAME)
            + ") VALUES (?,?,?)";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {

            for (User user : users) {
                statement.setString(1, user.getLogin());
                statement.setString(2, user.getPassword());
                statement.setString(3, new String(user.getName().getBytes(), StandardCharsets.UTF_8));
                statement.addBatch();
            }

            int[] resultSet = statement.executeBatch();
            if (resultSet.length == 0) {
                log.warn("Запрос на создание пользователей в количестве {} невыполнился", users.size());
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
        Collection<String> logins = users.stream().map(User::getLogin).toList();
        return getByLogins(logins);
    }

    private Collection<User> update(Collection<User> users) {

        if (users.isEmpty()) {
            return users;
        }

        String query = "UPDATE  " + TABLE_NAME + " SET " +
            UserMapper.FIELD_PASSWORD + "= ?, " +
            UserMapper.FIELD_NAME + "= ? " +
            "WHERE " + UserMapper.FIELD_ID + "= ?";

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query)) {

            for (User user : users) {
                statement.setString(1, user.getPassword());
                statement.setString(2, user.getName());
                statement.setLong(3, user.getId());
                statement.addBatch();
            }

            int[] resultSet = statement.executeBatch();
            if (resultSet.length == 0) {
                log.warn("Запрос на обновление пользователей в количестве {} невыполнился", users.size());
            }
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
        return users;
    }
}
