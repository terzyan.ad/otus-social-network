package net.devartel.api.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SurnameHolder {

    @JsonProperty("Surname")
    private String surname;
}
