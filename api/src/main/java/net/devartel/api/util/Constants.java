package net.devartel.api.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final String API_PATH = "/api";
    public static final String API_VERSION = "/v1";

    public static final String API_URL = API_PATH + API_VERSION;
}
