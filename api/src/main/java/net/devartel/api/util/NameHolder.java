package net.devartel.api.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class NameHolder {

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Sex")
    private String sex;
}
